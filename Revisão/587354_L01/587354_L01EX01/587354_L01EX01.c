#include <stdio.h>

int main()
{
	FILE *arq;

	char nome[20];
	char caract;

	scanf("%[^\n]", nome);

	arq = fopen(nome, "r");

	if (arq == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}

	while (fscanf(arq, "%c", &caract) != EOF)
		printf("%c", caract);

	fclose(arq);	
	
	return 0;
}