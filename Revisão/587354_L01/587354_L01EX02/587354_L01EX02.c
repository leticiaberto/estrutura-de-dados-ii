#include <stdio.h>

int main ()
{
	FILE *f1, *f2;
	char c, nome1[100], nome2[100];
	
	scanf("%[^\n]%*c", nome1);
	scanf("%[^\n]", nome2);
	
	f1 = fopen (nome1, "r");
	if (f1 == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	
	f2 = fopen (nome2, "w");
	if (f2 == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	
	while(fscanf(f1, "%c", &c) != EOF)
		fprintf (f2, "%c",c);
		
	fclose (f1);
	fclose(f2);
	
	return 0;
}