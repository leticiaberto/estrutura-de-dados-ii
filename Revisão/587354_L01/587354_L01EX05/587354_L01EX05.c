#include <stdio.h>

int main()
{
	FILE *org, *dest;

	int x1, x2;

	org = fopen ("arq.in", "r");

	dest = fopen ("arq.out","w");

	if (org == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}

	if (dest == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}

	while (fscanf(org, "%d %d\n", &x1, &x2) != EOF)
	{
		fprintf(dest, "%d %d ", x1, x2);
		fprintf(dest, "%d\n", x1 + x2);
	}

	fclose(org);
	fclose(dest);
	
	return 0;
}