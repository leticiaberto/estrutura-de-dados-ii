#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv)
{
	FILE *file, *fdic;
	
    char c, d, word[100], wordDic[100];
    int found, strcomparison;
	
	char nome[20];
	
	scanf("%[^\n]", nome);
	
    fdic = fopen("dicionario.dic", "r");
    file = fopen(nome, "r");

    if (file && fdic)
    {

        while ((c = fscanf(file, "%s", word)) != EOF)
        {
            found = 0;
            rewind(fdic);

            do
            {
                d = fscanf(fdic, "%s", wordDic);
                strcomparison = strcmp(word, wordDic);

                if (strcomparison == 0)
                {
                    found = 1;
                    d = fscanf(fdic, "%s", wordDic);
                    break;

                } else if (strcomparison < 0)
                    break;
                
                if (!found)
                    d = fscanf(fdic, "%s", wordDic);
            } while (d != EOF);

            if (found)
                printf("%s ", wordDic);
			else
                printf("%s ", word);
        }

        printf("\n");

    } else
        printf("Arquivo não existente\n");

    fclose(fdic);
    fclose(file);
    return 0;
}
