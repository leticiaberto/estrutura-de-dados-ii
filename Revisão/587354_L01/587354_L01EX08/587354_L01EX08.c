#include <stdio.h>

int main ()
{
	FILE *f;
	char nome[30];
	int tam = 0;

	scanf("%[^\n]", nome);

	f = fopen(nome, "r");

	if(f == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}

	fseek(f, 0, SEEK_END);
	tam = ftell(f);
	fclose(f);

	printf("%d\n", tam);
	return 0;
}