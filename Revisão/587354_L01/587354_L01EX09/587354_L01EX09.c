#include <stdio.h>
#include <stdlib.h>

typedef struct dados{
	char nome[30];
	char rua[40];
	int numero;
	float telefone; 
	char cidade[20];
	char estado[10];
	struct dados *prox;
}Dados;


/* Inicializa uma lista */
void inicLista(Dados *p_l){
	p_l->prox = NULL;
}

void insereInicio(Dados *p_l, char nome[30], char rua[40], int numero, float telefone, char cidade[20], char estado[10]){
	Dados *novo;
	int i;
	novo = malloc (sizeof(Dados));
	for (i = 0; nome[i] != '\0'; i++)
		novo->nome[i] = nome[i];
	for (i = 0; rua[i] != '\0'; i++)
		novo->rua[i] = rua[i];
	novo->numero = numero;
	novo->telefone = telefone;
	for (i = 0; cidade[i] != '\0'; i++)
		novo->cidade[i] = cidade[i];
	for (i = 0; estado[i] != '\0'; i++)
		novo->estado[i] = estado[i];

	novo->prox = NULL;
	while(p_l->prox != NULL)
		p_l = p_l->prox;
	p_l->prox = novo;		
}

/* Exibe o conteudo da lista */
void exibe(Dados *p_l){
	while(p_l->prox != NULL){
		puts(p_l->prox->nome);
		puts(p_l->prox->rua);
		printf("%d\n",p_l->prox->numero);
		printf("%.0f\n",p_l->prox->telefone);
		puts(p_l->prox->cidade);
		puts(p_l->prox->estado);
		printf("------------------------\n");
		p_l = p_l->prox;
	}
}

int main()
{
	FILE *f;
	Dados l;

	char nome[30];
	char rua[40];
	int numero;
	float telefone; 
	char cidade[20];
	char estado[10];

	inicLista(&l);

	f = fopen("arq.in", "r");

	if (f == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}

	while(fscanf(f, "%[^\n]\n%[^\n]\n%d\n%f\n%[^\n]\n%[^\n]\n",nome,rua, &numero, &telefone,cidade, estado) != EOF)
	{	
		insereInicio(&l,nome,rua, numero, telefone,cidade, estado);
	}
	
	exibe(&l);

	fclose(f);

	return 0;
}