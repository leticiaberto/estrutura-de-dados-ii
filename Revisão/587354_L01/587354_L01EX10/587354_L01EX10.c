#include <stdio.h>
#include <stdlib.h>

typedef struct no{
  int idade;
  char sexo;
  float salario;
  char estCivil[10];
  int dependentes;
  float valPatri;
  float qntdCal;
  char grauInst[30];
  struct no *prox;
} Lista;

/* Inicializa uma lista */
void inicLista(Lista *p_l){
	p_l->prox = NULL;
}

/* Insere um elemento no final da lista */
void insereFim(Lista *p_l, int idade, char sexo, float salario, char estCivil [], int dependentes, float valPatri, float qntdCal, char grauInst []){
	Lista *novo;
	novo = malloc (sizeof(Lista));
	novo->idade = idade;
	novo->sexo = sexo;
	novo->salario = salario;
	int i;
	for (i = 0; estCivil[i] != '\0'; i++)
		novo->estCivil[i] = estCivil[i]; 
	novo->dependentes = dependentes;
	novo->valPatri = valPatri;
	novo->qntdCal = qntdCal;
	for (i = 0; grauInst[i] != '\0'; i++)
		novo->grauInst[i] = grauInst[i];
	novo->prox = NULL;
	
	while(p_l->prox != NULL)
		p_l = p_l->prox;
	p_l->prox = novo;	
}

void salva(Lista *p_l, FILE **f4){
	while(p_l->prox != NULL){
		fprintf(*f4, "%d\n%c\n%.2f\n%s\n%d\n%.2f\n%.2f\n%s\n\n", p_l->prox->idade, p_l->prox->sexo, p_l->prox->salario, p_l->prox->estCivil, p_l->prox->dependentes, p_l->prox->valPatri, p_l->prox->qntdCal, p_l->prox->grauInst);
		p_l = p_l->prox;
		
	}
}
void intercala(Lista *l1, Lista *l2, Lista *l3, Lista *l4)
{
	while (l1->prox != NULL || l2->prox != NULL || l3->prox != NULL)
	{
		if (l1->prox != NULL && l2->prox != NULL && l3->prox != NULL)
		{
			if (l1->prox->idade <= l2->prox->idade && l1->prox->idade <= l3->prox->idade)
			{
				insereFim(l4, l1->prox->idade, l1->prox->sexo, l1->prox->salario, l1->prox->estCivil, l1->prox->dependentes, l1->prox->valPatri, l1->prox->qntdCal, l1->prox->grauInst);	
				l1 = l1->prox;
			}
			else if (l2->prox->idade <= l3->prox->idade && l2->prox->idade <= l1->prox->idade)
				{
					insereFim(l4, l2->prox->idade, l2->prox->sexo, l2->prox->salario, l2->prox->estCivil, l2->prox->dependentes, l2->prox->valPatri, l2->prox->qntdCal, l2->prox->grauInst);	
					l2 = l2->prox;
				}
				else if (l3->prox->idade <= l1->prox->idade && l3->prox->idade <= l2->prox->idade)
					{
						insereFim(l4, l3->prox->idade, l3->prox->sexo, l3->prox->salario, l3->prox->estCivil, l3->prox->dependentes, l3->prox->valPatri, l3->prox->qntdCal, l3->prox->grauInst);	
						l3 = l3->prox;
					}
				
		}
		else if(l1->prox != NULL && l2->prox != NULL && l3->prox == NULL)
			 {
				if (l1->prox->idade <= l2->prox->idade)
				{
					insereFim(l4, l1->prox->idade, l1->prox->sexo, l1->prox->salario, l1->prox->estCivil, l1->prox->dependentes, l1->prox->valPatri, l1->prox->qntdCal, l1->prox->grauInst);	
					l1 = l1->prox;
				}
				else
				{
					insereFim(l4, l2->prox->idade, l2->prox->sexo, l2->prox->salario, l2->prox->estCivil, l2->prox->dependentes, l2->prox->valPatri, l2->prox->qntdCal, l2->prox->grauInst);	
					l2 = l2->prox;
				}	
			 }
			else if(l1->prox != NULL && l2->prox == NULL && l3->prox != NULL)
				 {
					if (l1->prox->idade <= l3->prox->idade)
					{
						insereFim(l4, l1->prox->idade, l1->prox->sexo, l1->prox->salario, l1->prox->estCivil, l1->prox->dependentes, l1->prox->valPatri, l1->prox->qntdCal, l1->prox->grauInst);	
						l1 = l1->prox;
					}
					else
					{
						insereFim(l4, l3->prox->idade, l3->prox->sexo, l3->prox->salario, l3->prox->estCivil, l3->prox->dependentes, l3->prox->valPatri, l3->prox->qntdCal, l3->prox->grauInst);	
						l3 = l3->prox;
					}	
			 	}
			 	else if(l2->prox != NULL && l3->prox != NULL && l1->prox == NULL)
			 		 {
						if (l2->prox->idade <= l3->prox->idade)
						{
							insereFim(l4, l2->prox->idade, l2->prox->sexo, l2->prox->salario, l2->prox->estCivil, l2->prox->dependentes, l2->prox->valPatri, l2->prox->qntdCal, l2->prox->grauInst);	
							l2 = l2->prox;
						}
						else
						{
							insereFim(l4, l3->prox->idade, l3->prox->sexo, l3->prox->salario, l3->prox->estCivil, l3->prox->dependentes, l3->prox->valPatri, l3->prox->qntdCal, l3->prox->grauInst);	
							l3 = l3->prox;
						}	
			 		 }
			 		 else if(l2->prox != NULL && l1->prox == NULL && l3->prox == NULL)
			 		 	  {
							insereFim(l4, l2->prox->idade, l2->prox->sexo, l2->prox->salario, l2->prox->estCivil, l2->prox->dependentes, l2->prox->valPatri, l2->prox->qntdCal, l2->prox->grauInst);	
							l2 = l2->prox;
			 		 	  }
			 		 	  else if(l1->prox != NULL && l2->prox == NULL && l3->prox == NULL)
			 		 	  {
							insereFim(l4, l1->prox->idade, l1->prox->sexo, l1->prox->salario, l1->prox->estCivil, l1->prox->dependentes, l1->prox->valPatri, l1->prox->qntdCal, l1->prox->grauInst);	
							l1 = l1->prox;
			 		 	  }
			 		 	  else
			 		 	  {
							insereFim(l4, l3->prox->idade, l3->prox->sexo, l3->prox->salario, l3->prox->estCivil, l3->prox->dependentes, l3->prox->valPatri, l3->prox->qntdCal, l3->prox->grauInst);	
							l3 = l3->prox;
			 		 	  }

	}
}

int main()
{
	FILE *f1, *f2, *f3, *f4;

	int idade;
	char sexo;
	float salario;
	char estCivil[10];
	int dependentes;
	float valPatri;
	char grauInst[30];
	float qntdCal;

	Lista l1, l2, l3, l4;

  	inicLista(&l1);
  	inicLista(&l2);
  	inicLista(&l3);
  	inicLista(&l4);

	f1 = fopen("reg1.dat", "r");
	f2 = fopen("reg2.dat", "r");
	f3 = fopen("reg3.dat", "r");
	f4 = fopen("regAll.dat", "w");

	if(f1 == NULL)
	{
		perror("Erro ao abrir o arquivo 1");
		return 1;
	}

	if(f2 == NULL)
	{
		perror("Erro ao abrir o arquivo 2");
		return 1;
	}

	if(f3 == NULL)
	{
		perror("Erro ao abrir o arquivo 3");
		return 1;
	}

	if(f4 == NULL)
	{
		perror("Erro ao abrir o arquivo resultante");
		return 1;
	}

	while(fscanf(f1, "%d %c %f %[^\n] %d %f %f %[^\n]", &idade, &sexo, &salario, estCivil, &dependentes, &valPatri, &qntdCal, grauInst) != EOF)
		insereFim(&l1, idade, sexo, salario, estCivil, dependentes, valPatri, qntdCal, grauInst);

	while(fscanf(f2, "%d %c %f %[^\n] %d %f %f %[^\n]", &idade, &sexo, &salario, estCivil, &dependentes, &valPatri, &qntdCal, grauInst) != EOF)
		insereFim(&l2, idade, sexo, salario, estCivil, dependentes, valPatri, qntdCal, grauInst);

	while(fscanf(f3, "%d %c %f %[^\n] %d %f %f %[^\n]", &idade, &sexo, &salario, estCivil, &dependentes, &valPatri, &qntdCal, grauInst) != EOF)
		insereFim(&l3, idade, sexo, salario, estCivil, dependentes, valPatri, qntdCal, grauInst);
	
	intercala(&l1, &l2, &l3, &l4);
	
	salva(&l4, &f4);
	
	fclose(f1);
	fclose(f2);
	fclose(f3);
	fclose(f4);

	return 0;
}