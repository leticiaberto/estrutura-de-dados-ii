/* Nome: Letícia Mara Berto
RA: 587354 */
#include <stdio.h>
#define MAX 5000
int main()
{
	FILE *f;
	char nome[255];
	char c;
	scanf("%[^\n]", nome);
	
	f = fopen(nome, "rb");
	if (f == NULL)
	{
		perror ("Erro ao abrir o arquivo");
		return 1;
		
	}
	while (!feof(f))
	{
    	fread(&c, sizeof(char), 1, f);
    	if (feof(f)) break;
   		printf("%c", c);
 	} 

	fclose(f);
	return 0;
}
