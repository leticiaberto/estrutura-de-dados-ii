#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct no{
  int x;
  struct no *prox;
} Lista;

//Quicksorte nativo da linguagem C com função de comparar em ordem crescente

int compara (const void *a, const void *b)
{
	if (*(int*)a == *(int*)b)
		return 0;
	else if (*(int*)a < *(int*)b)
		return -1;//vem antes
		else
			return 1;
}

void inicLista(Lista *p_l){
	p_l->prox = NULL;
}

/* Insere um elemento no final da lista */
void insereFim(Lista *p_l, int e){
	Lista *novo;
	novo = malloc (sizeof(Lista));
	novo->x = e;
	novo->prox = NULL;
	
	while(p_l->prox != NULL)
		p_l = p_l->prox;
	p_l->prox = novo;	
}

void salva(Lista *p_l, FILE **f){
	while(p_l->prox != NULL){
		fprintf(*f, "%d ", p_l->prox->x);
		p_l = p_l->prox;
		
	}
}

int main()
{
	int n, i;
	int vet[100];
	Lista l;
	FILE *f;

	inicLista(&l);

	f = fopen("arq.out", "w");

	if (f == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}

	scanf("%d", &n);
	
	srand(time(NULL));//Semente da aleatoriedade

	for (i = 0; i < n; i++)
		vet[i] = rand() % 100;

/*for (i = 0; i < n; i++)
		printf("%d ",vet[i]); */

	qsort (vet, n, sizeof(int), compara);

	for (i = 0; i < n; i++)
		insereFim(&l, vet[i]);

	fprintf(f, "%d\n",n);
	salva(&l,&f);

	fclose(f);
	return 0;
}