#include <stdio.h>
#include <stdlib.h>

typedef struct no{
  int info;
  struct no *prox;
  struct no *ant;
} No_lista;

typedef No_lista* Lista;

void inicLista(Lista *p_l){
	*p_l = NULL;
}

/* Insere um elemento no final da lista */
void insereFim(Lista *p_l, int e){
	No_lista *novo;
	Lista aux;
	
	novo = malloc (sizeof(No_lista));
	novo->info = e;
	novo->prox = NULL;
	
	aux = *p_l;
	if (aux == NULL)//lista vazia
	{
		novo->ant = NULL;
		*p_l = novo;
	}
	else {
		while(aux->prox != NULL)
			aux = aux->prox;
		aux->prox = novo;	
		novo->ant = aux;
	}
}

//Para remover o nó que contem o tamanho da lista
int removeInicio(Lista *p_l){
	No_lista *aux;
	aux = *p_l;
	if (aux == NULL)//lista vazia
		return 0;
	if (aux->prox == NULL)//tem só um elemento
	{
		*p_l = NULL;
		free(aux);
	}
	else
	{	
		*p_l = aux->prox;
		(*p_l)->ant = NULL;
		free (aux);
	}
	return 1;
}

/* Exibe o conteudo da lista */
void exibe(Lista *p_l){
	Lista aux;
	aux = *p_l;
	while(aux != NULL){
		printf("%d\n", aux->info);
		aux = aux->prox;
	}
}

/* Inverte os elementos de uma lista */
void inverte(Lista *p_l){
	No_lista *aux;
	
	// lista vazia
	if (*p_l == NULL)
		return;

	// lista com um unico elemento
	if ((*p_l)->prox == NULL)
		return;
		
	while(*p_l != NULL){
		aux = (*p_l)->ant;
		(*p_l)->ant = (*p_l)->prox;
		(*p_l)->prox = aux;
		aux = *p_l;
		*p_l = (*p_l)->ant;
	}
	*p_l = aux;
}

int Menor(Lista *p_l)
{
	return (*p_l)->info;
}

int Maior(Lista *p_l)
{
	Lista aux;
	aux = *p_l;

	while(aux->prox != NULL)
		aux = aux->prox;
	return(aux->info);
}

int main()
{
	int x;
	FILE *f;
	Lista l;
	int maior, menor;

	inicLista(&l);

	f = fopen("arq.out", "r");

	if (f == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}

	while(fscanf(f, "%d", &x) != EOF)
		insereFim(&l, x);

	removeInicio(&l);

	menor = Menor(&l);
	maior = Maior(&l);
	//Menor = Menor(&l);

	inverte(&l);
	exibe(&l);

	fclose(f);
	printf("Menor: %d\n", menor);
	printf("Maior: %d\n",maior);

	return 0;
}