#include <stdio.h>
#include <time.h>
#include <stdlib.h>

FILE *f;

typedef struct no {
  int info; /* chave */
  struct no *esq, *dir;
} No;

typedef No* Arvore;

void cria_arvore(Arvore *p){
	*p = NULL;
}

/* Retorna 0 se a chave for repetida */
int  insere(Arvore *p, int chave) {
	No *novo;
	
	if(*p == NULL){ // caso base
		novo = malloc (sizeof(No));
		novo->info = chave;
		novo->esq = NULL;
		novo->dir = NULL;
		*p = novo;
		return 1;
	}
	if (chave < (*p)->info)
		return insere(&((*p)->esq), chave);
	else if (chave > (*p)->info)
		return insere(&(*p)->dir, chave);
	return 0;
}

void inorder(Arvore p) {
	if (p != NULL) {
		inorder (p->esq);
		fprintf(f,"%d ", p->info);    
		inorder (p->dir);    
	}
}

int main()
{
	int n, x, i;

	scanf("%d",&n);

	Arvore a;
	cria_arvore(&a);

	srand(time(NULL));//Semente da aleatoriedade

	for (i = 0; i < n; i++)
	{	
		x = rand() % 100;
		insere(&a, x);
	}

	

	f = fopen("arq.out", "w");

	if (f == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}

	inorder(a);

	fclose(f);

	f = fopen("arq.out", "r");

	if (f == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}

	float soma = 0;
	while (fscanf(f, "%d", &x) != EOF)
	{
		int flag = 0;
		for (i = 2; i < x; i++)
			if (x % i == 0)
				flag = 1;
		if (flag == 0)
			soma = soma + x;
	}	

	printf("Soma dos primos: %.0f\n", soma);
	fclose(f);	
	return 0;
}