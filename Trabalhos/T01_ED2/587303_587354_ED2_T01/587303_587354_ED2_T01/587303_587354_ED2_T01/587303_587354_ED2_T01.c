#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "587303_587354_ED2_T01.h"


//funcao que verifica a existencia do arquivo de dados e dos indices, e os carrega na RAM
void inicializar(indicePrimario p_l[], indiceWinner p_w[], indiceMVP p_m[], int *tam_p, int *tam_w, int *tam_m, int *indice) {


	int flag;
	FILE *primary, *winner, *mvp, *data;

	data = fopen("matches.dat", "r+");
	if (data == NULL) {
		//criar arquivo de dados no disco e abrir para leitura e escrita
		data = fopen("matches.dat", "w+");
	}

	primary = fopen("iprimary.idx", "r+");
	if (primary == NULL) {
		//criar arquivo de dados no disco e abrir para leitura e escrita
		primary = fopen("iprimary.idx", "w+");
	}

	if(primary != NULL) {
		fseek(primary, 0, SEEK_END);
		int file_size = ftell(primary);
		rewind(primary);

		if (file_size != 0)//Arquivo já existia
		{
			fscanf(primary, "%d", &flag);
			if (flag) {
				carregarIndicePrimario(primary, p_l, tam_p);
				fclose(primary);
			} else {
				refazerIndicesPrimarios(p_l,tam_p, data, indice);
				fclose(primary);
				salvarIndicePrimario(p_l, tam_p);
			}
		}
		else//Arq foi criado agora
		{
			refazerIndicesPrimarios(p_l,tam_p, data, indice);
			int flagw = 0;
			fprintf(primary, "%d\n", flagw);
			fclose(primary);
			salvarIndicePrimario(p_l, tam_p);
		}


	}

	winner = fopen("iwinner.idx", "r+");
	if(winner == NULL) {
		winner = fopen("iwinner.idx", "w+");
	}
	if(winner != NULL) {
		fseek(winner, 0, SEEK_END);
		int file_size = ftell(winner);
		rewind(winner);

		if (file_size != 0)//Arquivo já existia
		{
			fscanf(winner, "%d", &flag);
			if (flag) {
				carregarIndiceWinner(winner, p_w, tam_w);
				fclose(winner);

			} else {
				refazerIndicesWinner(p_w, tam_w, data);
				fclose(winner);
				salvarIndiceWinner(p_w, tam_w);
			}
		}
		else//Arq foi criado agora
		{

			refazerIndicesWinner(p_w, tam_w, data);
			int flagw = 0;
			fprintf(winner, "%d\n", flagw);
			fclose(winner);
			salvarIndiceWinner(p_w, tam_w);
		}

	}

	mvp = fopen("imvp.idx", "r+");
	if(mvp == NULL) {
		mvp = fopen("imvp.idx", "w+");
	}
	if(mvp != NULL) {
		fseek(mvp, 0, SEEK_END);
		int file_size = ftell(mvp);
		rewind(mvp);

		if (file_size != 0)//Arquivo já existia
		{
			fscanf(mvp, "%d", &flag);
			if (flag) {
				carregarIndiceMVP(mvp, p_m, tam_m);
				fclose(mvp);

			} else {
				refazerIndicesMVP(p_m, tam_m, data);
				fclose(mvp);
				salvarIndiceMVP(p_m, tam_m);
			}
		}
		else//Arq foi criado agora
		{
			refazerIndicesMVP(p_m, tam_m, data);
			int flagw = 0;
			fprintf(mvp, "%d\n", flagw);
			fclose(mvp);
			salvarIndiceMVP(p_m, tam_m);
		}


	}

	fclose(data);

}

void salvarIndicePrimario(indicePrimario pri[], int *tam_p) {
	int i;
	int flag = 1, auxFlag;

	FILE *primarioFile = fopen("iprimary.idx", "r+");

	rewind(primarioFile);
	fscanf(primarioFile, "%d\n", &auxFlag);
	fclose(primarioFile);
	if(auxFlag == 0){
		primarioFile = fopen("iprimary.idx", "w+");
		rewind(primarioFile);
		fprintf(primarioFile, "%d\n", flag);

		for (i = 0; i < *tam_p; i++) {
			fprintf(primarioFile, "%s@", pri[i].pk);
			fprintf(primarioFile, "%d\n", pri[i].rrn);
		}
		fclose(primarioFile);
	}

}

void salvarIndiceWinner(indiceWinner wi[], int *tam_w) {
	int i;
	int flag = 1, auxFlag;

	FILE *winnerFile = fopen("iwinner.idx", "r+");

	rewind(winnerFile);
	fscanf(winnerFile, "%d\n", &auxFlag);
	fclose(winnerFile);
	if(auxFlag == 0){
		winnerFile = fopen("iwinner.idx", "w+");
		rewind(winnerFile);
		fprintf(winnerFile, "%d\n", flag);

		for (i = 0; i < *tam_w; i++) {
			fprintf(winnerFile, "%s@%s\n", wi[i].vencedora, wi[i].pk_registro);
		}
		fclose(winnerFile);
	}
}

void salvarIndiceMVP(indiceMVP mvp[], int *tam_m) {
	int i;
	int flag = 1, auxFlag;

	FILE *mvpFile = fopen("imvp.idx", "r+");

	rewind(mvpFile);
	fscanf(mvpFile, "%d\n", &auxFlag);
	fclose(mvpFile);
	if(auxFlag == 0){
		mvpFile = fopen("imvp.idx", "w+");
		rewind(mvpFile);
		fprintf(mvpFile, "%d\n", flag);

		for (i = 0; i < *tam_m; i++) {
			fprintf(mvpFile, "%s@%s\n", mvp[i].apelido, mvp[i].pkRegistro);
		}
		fclose(mvpFile);
	}

}

/*Cria os vetores a partir do arquivo de dados. Não existe nenhum arquivo de indices ou estão desatualizados (flag == 0)*/
void refazerIndicesPrimarios (indicePrimario pri[], int *tam_p, FILE *data, int *indice) {

	Partida *part;
	int tamRegistro = 36;	//comeca com 36 porque eh o tamanho dos campos fixos + @
	int aux;

	*tam_p = *indice = 0;

	part = (Partida *)malloc(sizeof(Partida));

	rewind(data);


	while (fscanf(data, "%[^@]@", part->codigo) != EOF) {
		tamRegistro = 36;
		fscanf(data, "%[^@]@", part->nomeAzul);
		fscanf(data, "%[^@]@", part->nomeVermelha);
		fscanf(data, "%[^@]@", part->data);
		fscanf(data, "%[^@]@", part->duracao);
		fscanf(data, "%[^@]@", part->nomeVencedora);
		fscanf(data, "%[^@]@", part->placarAzul);
		fscanf(data, "%[^@]@", part->placarVermelha);
		fscanf(data, "%[^@]@", part->apelidoMVP);



		listaPrimario(pri, part, tam_p, 0);


		tamRegistro = tamRegistro + strlen(part->nomeAzul) + strlen(part->nomeVermelha) + strlen(part->nomeVencedora) + strlen(part->apelidoMVP);
		aux = 192 - tamRegistro; //quantos # eu terei que ler e ignorar
		fseek(data, aux, SEEK_CUR);

		(*tam_p)++;
		(*indice)++;
	}


}

void refazerIndicesWinner (indiceWinner w[], int *tam_w, FILE *data) {

	Partida *part;
	int tamRegistro = 36;	//comeca com 36 porque eh o tamanho dos campos fixos + @
	int aux;

	*tam_w = 0;

	part = (Partida *)malloc(sizeof(Partida));


	rewind(data);

	while (fscanf(data, "%[^@]@", part->codigo) != EOF) {
		tamRegistro = 36;
		fscanf(data, "%[^@]@", part->nomeAzul);
		fscanf(data, "%[^@]@", part->nomeVermelha);
		fscanf(data, "%[^@]@", part->data);
		fscanf(data, "%[^@]@", part->duracao);
		fscanf(data, "%[^@]@", part->nomeVencedora);
		fscanf(data, "%[^@]@", part->placarAzul);
		fscanf(data, "%[^@]@", part->placarVermelha);
		fscanf(data, "%[^@]@", part->apelidoMVP);

		listaWinner(w, part, tam_w, 0);

		tamRegistro = tamRegistro + strlen(part->nomeAzul) + strlen(part->nomeVermelha) + strlen(part->nomeVencedora) + strlen(part->apelidoMVP);
		aux = 192 - tamRegistro; //quantos # eu terei que ler e ignorar
		fseek(data, aux, SEEK_CUR);

		(*tam_w)++;
	}
}

void refazerIndicesMVP (indiceMVP m[], int *tam_m, FILE *data) {

	Partida *part;
	int tamRegistro = 36;	//comeca com 36 porque eh o tamanho dos campos fixos + @
	int aux;

	*tam_m = 0;

	part = (Partida *)malloc(sizeof(Partida));


	rewind(data);

	while (fscanf(data, "%[^@]@", part->codigo) != EOF) {
		tamRegistro = 36;
		fscanf(data, "%[^@]@", part->nomeAzul);
		fscanf(data, "%[^@]@", part->nomeVermelha);
		fscanf(data, "%[^@]@", part->data);
		fscanf(data, "%[^@]@", part->duracao);
		fscanf(data, "%[^@]@", part->nomeVencedora);
		fscanf(data, "%[^@]@", part->placarAzul);
		fscanf(data, "%[^@]@", part->placarVermelha);
		fscanf(data, "%[^@]@", part->apelidoMVP);

		listaMVP(m, part, tam_m, 0);

		tamRegistro = tamRegistro + strlen(part->nomeAzul) + strlen(part->nomeVermelha) + strlen(part->nomeVencedora) + strlen(part->apelidoMVP);
		aux = 192 - tamRegistro; //quantos # eu terei que ler e ignorar
		fseek(data, aux, SEEK_CUR);

		(*tam_m)++;
	}
}

void alterar(char pk[], indicePrimario p_l[], int tam_p, Partida p[]) {

	int flag, rrn, posicao;
	char duracao[6];
	FILE *dado;
	Partida *part;

	dado = fopen("matches.dat", "r+");

	posicao = buscaBinaria(p_l, pk, 0, tam_p);	//busca binaria retorna a posicao do vetor
	if (posicao == -1) {
		printf("Registro não encontrado!\n");
		return;
	}

	rrn = p_l[posicao].rrn;

	part = (Partida *)malloc(sizeof(Partida));
	fseek(dado, rrn, SEEK_SET);
	fscanf(dado, "%[^@]@", part->codigo);
	fscanf(dado, "%[^@]@", part->nomeAzul);
	fscanf(dado, "%[^@]@", part->nomeVermelha);
	fscanf(dado, "%[^@]@", part->data);

	if(rrn != -1) {
		flag = 0;
		while(flag == 0) {
			scanf("%[^\n]%*c", duracao);
			if (strlen(duracao) != 5 || duracao[2] != ':') {
				printf("Campo Inválido!\n");
				flag = 0;
			}
			else {
				flag = 1;
				duracao[5] = '\0';
				//escrever diretamente no arquivo de dados
				fprintf(dado, "%s", duracao);

			}

		}
	} else {
		printf("Registro não encontrado!\n");
	}

	fclose(dado);
}

//nos arquivos indices, como nao foi especificado, coloquei o @ como separador de campos e um registro por linha
void carregarIndicePrimario(FILE *primary, indicePrimario p_l[], int *tam_p) {

	char string[9];
	int rrn, i = 0;

	fseek(primary, 2, SEEK_SET);

	while(fscanf(primary, "%[^@]@", string) != EOF) {
		strcpy(p_l[i].pk, string);
		fscanf(primary, "%d\n", &rrn);
		p_l[i].rrn = rrn;
		i++;
		(*tam_p)++;

	}

}

void carregarIndiceWinner(FILE *winner, indiceWinner p_w[], int *tam_w) {

	char string[39];
	char pkStr[9];
	int i = 0;

	fseek(winner, 2, SEEK_SET);	//andar 4 bytes do inicio do arquivo devido a flag (int)

	while(fscanf(winner, "%[^@]@%[^\n]%*c", string, pkStr) != EOF) {
		strcpy(p_w[i].vencedora, string);
		strcpy(p_w[i].pk_registro, pkStr);
		i++;
		(*tam_w)++;

	}

}

void carregarIndiceMVP(FILE *mvp, indiceMVP p_m[], int *tam_m) {

	char string[39];
	char pkStr[9];
	int i = 0;

	fseek(mvp, 2, SEEK_SET);	//andar 4 bytes do inicio do arquivo devido a flag (int)

	while(fscanf(mvp, "%[^@]@%[^\n]%*c", string, pkStr) != EOF) {
		strcpy(p_m[i].apelido, string);
		strcpy(p_m[i].pkRegistro, pkStr);
		i++;
		(*tam_m)++;

	}

}

int verificaPKExistente(indicePrimario p_l[], Partida p[], int *tam_p, int indice)
{
	int i;
	for (i = 0; i < *tam_p; i++)
	{
		if (strcmp (p[indice].codigo, p_l[i].pk) == 0)
			return 1;
	}
	return 0;
}

//bloco da criacao das estruturas (listas) dos tres tipos de indices
void listaPrimario (indicePrimario p_l[], Partida p[], int *tam_p, int indice) {

	int rrn;

	rrn = *tam_p * 192;
	int i = 0, j;
	if(*tam_p != 0){
		for (i = 0; i < *tam_p; i++)
		{
			if (strcmp (p[indice].codigo, p_l[i].pk) < 0)
			{
				for (j = *tam_p; j >= i; j--)
				{
					strcpy(p_l[j+1].pk, p_l[j].pk);
					p_l[j+1].rrn = p_l[j].rrn;
				}
				strcpy(p_l[i].pk, p[indice].codigo);
				p_l[i].rrn = rrn;
				break;
			}
		}
		if(i == *tam_p){
			strcpy(p_l[i].pk, p[indice].codigo);
			p_l[i].rrn = rrn;
		}
	}
	else
	{
		strcpy(p_l[i].pk, p[indice].codigo);
		p_l[i].rrn = rrn;
	}
}

void listaWinner (indiceWinner p_w[], Partida p[], int *tam_w, int indice) {

	int i = 0, j;
	if(*tam_w != 0){
		for (i = 0; i < *tam_w; i++)
		{
			if (strcasecmp (p[indice].nomeVencedora, p_w[i].vencedora) < 0)
			{
				for (j = *tam_w; j >= i; j--)
				{
					strcpy(p_w[j+1].vencedora, p_w[j].vencedora);
					strcpy(p_w[j+1].pk_registro, p_w[j].pk_registro);
				}
				strcpy(p_w[i].vencedora, p[indice].nomeVencedora);
				strcpy(p_w[i].pk_registro, p[indice].codigo);
				break;
			} else if (strcasecmp (p[indice].nomeVencedora, p_w[i].vencedora) == 0)
			{
				if (strcmp (p[indice].codigo, p_w[i].pk_registro) < 0) {
					for (j = *tam_w; j >= i; j--)
					{
						strcpy(p_w[j+1].vencedora, p_w[j].vencedora);
						strcpy(p_w[j+1].pk_registro, p_w[j].pk_registro);
					}
					strcpy(p_w[i].vencedora, p[indice].nomeVencedora);
					strcpy(p_w[i].pk_registro, p[indice].codigo);
					break;
				}
			}
		}
		if(i == *tam_w){
			strcpy(p_w[i].vencedora, p[indice].nomeVencedora);
			strcpy(p_w[i].pk_registro, p[indice].codigo);
		}
	}
	else
	{
		strcpy(p_w[i].vencedora, p[indice].nomeVencedora);
		strcpy(p_w[i].pk_registro, p[indice].codigo);
	}
}

void listaMVP (indiceMVP p_m[], Partida p[], int *tam_m, int indice) {

	int i = 0, j;
	if(*tam_m != 0){
		for (i = 0; i < *tam_m; i++)
		{
			if (strcasecmp (p[indice].apelidoMVP, p_m[i].apelido) < 0)
			{
				for (j = *tam_m; j >= i; j--)
				{
					strcpy(p_m[j+1].apelido, p_m[j].apelido);
					strcpy(p_m[j+1].pkRegistro, p_m[j].pkRegistro);
				}
				strcpy(p_m[i].apelido, p[indice].apelidoMVP);
				strcpy(p_m[i].pkRegistro, p[indice].codigo);
				break;
			} else if (strcasecmp (p[indice].apelidoMVP, p_m[i].apelido) == 0)
			{
				if (strcmp (p[indice].codigo, p_m[i].pkRegistro) < 0) {
					for (j = *tam_m; j >= i; j--)
					{
						strcpy(p_m[j+1].apelido, p_m[j].apelido);
						strcpy(p_m[j+1].pkRegistro, p_m[j].pkRegistro);
					}
					strcpy(p_m[i].apelido, p[indice].apelidoMVP);
					strcpy(p_m[i].pkRegistro, p[indice].codigo);
					break;
				}
			}
		}
		if(i == *tam_m){
			strcpy(p_m[i].apelido, p[indice].apelidoMVP);
			strcpy(p_m[i].pkRegistro, p[indice].codigo);
		}
	}
	else
	{
		strcpy(p_m[i].apelido, p[indice].apelidoMVP);
		strcpy(p_m[i].pkRegistro, p[indice].codigo);
	}
}
//fim do bloco


void cadastro(Partida p[], int tam, indicePrimario p_l[], indiceWinner p_w[], indiceMVP p_m[], int *tam_p, int indice, int *tam_w, int *tam_m) {
	int i;

	FILE *dados;

	dados = fopen ("matches.dat", "a+");

	if (dados == NULL) {
		printf("Arquivo nao encontrado\n");
		return;
	} else {
		fprintf(dados, "%s@", p[indice].codigo);
		fprintf(dados, "%s@", p[indice].nomeAzul);
		fprintf(dados, "%s@", p[indice].nomeVermelha);
		fprintf(dados, "%s@", p[indice].data);
		fprintf(dados, "%s@", p[indice].duracao);
		fprintf(dados, "%s@", p[indice].nomeVencedora);
		fprintf(dados, "%s@", p[indice].placarAzul);
		fprintf(dados, "%s@", p[indice].placarVermelha);
		fprintf(dados, "%s@", p[indice].apelidoMVP);

		for (i = tam; i < 192; i++)
			fprintf(dados, "#");

		listaPrimario(p_l, p, tam_p, indice);
		listaWinner(p_w, p, tam_w, indice);
		listaMVP(p_m, p, tam_m, indice);

	}

	fclose(dados);

	int flag = 0;
	FILE *primarioFile = fopen("iprimary.idx", "r+");
	fprintf(primarioFile, "%d\n", flag);
	fclose(primarioFile);

	FILE *winnerFile = fopen("iwinner.idx", "r+");
	fprintf(winnerFile, "%d\n", flag);
	fclose(winnerFile);

	FILE *mvpFile = fopen("imvp.idx", "r+");
	fprintf(mvpFile, "%d\n", flag);
	fclose(mvpFile);

}

int remocao(char pk[9], indicePrimario p_l[], int tam_p) {

	int rrn;
	int i = buscaBinaria(p_l, pk, 0, tam_p);
	if (i == -1)
		return 0;
	else
	{
		rrn = p_l[i].rrn;
		p_l[i].rrn = -1;

		FILE *file = fopen("matches.dat", "r+");
		fseek(file, rrn, SEEK_SET);
		fprintf(file, "%s", "*|");
		fclose(file);

		int flag = 0;
		FILE *primarioFile = fopen("iprimary.idx", "r+");
		fprintf(primarioFile, "%d\n", flag);
		fclose(primarioFile);

		return 1;
	}

}

void liberarEspaco(indicePrimario p_l[], indiceMVP p_m[], indiceWinner p_w[], int tam_p, int tam_w, int tam_m, int *indice){

	FILE *dados, *dadosNovo;
	int flag = 0, i, count = 0;
	char string[192];

	dados = fopen("matches.dat", "r");
	dadosNovo = fopen("matchesold.dat", "w+");
	for (i = 0; i < tam_p; i++) {
		string[192] = '\0';
		fscanf(dados, "%192c", string);
		if(string[0] == '*' && string[1] == '|')
			count++;
		else {
			fprintf(dadosNovo, "%s", string);
		}
	}

	if (count == 0) { // Não existem registros deletados, então não precisa prosseguir
		fclose(dados);
		fclose(dadosNovo);

		remove("matchesold.dat");

		return;
	}


	FILE *primarioFile = fopen("iprimary.idx", "r+");
	fprintf(primarioFile, "%d\n", flag);

	FILE *winnerFile = fopen("iwinner.idx", "r+");
	fprintf(winnerFile, "%d\n", flag);

	FILE *mvpFile = fopen("imvp.idx", "r+");
	fprintf(mvpFile, "%d\n", flag);


	refazerIndicesPrimarios(p_l, &tam_p, dadosNovo, indice);
	fclose(primarioFile);
	salvarIndicePrimario(p_l, &tam_p);

	refazerIndicesWinner(p_w, &tam_w, dadosNovo);
	fclose(winnerFile);
	salvarIndiceWinner(p_w, &tam_w);

	refazerIndicesMVP(p_m, &tam_m, dadosNovo);
	fclose(mvpFile);
	salvarIndiceMVP(p_m, &tam_m);


	fclose(dados);
	fclose(dadosNovo);

	remove("matches.dat");
	rename("matchesold.dat", "matches.dat");
	remove("matchesold.dat");
}

void imprimeRegistro(int rrn, Partida p[])
{
	FILE *dados;
	int i = 0;

	dados = fopen("matches.dat", "r");
	if (dados != NULL)
	{
		fseek(dados, rrn, SEEK_SET);

		fscanf(dados, "%[^@]@", p[i].codigo );
		fscanf(dados, "%[^@]@", p[i].nomeAzul );
		fscanf(dados, "%[^@]@", p[i].nomeVermelha );
		fscanf(dados, "%[^@]@", p[i].data );
		fscanf(dados, "%[^@]@", p[i].duracao );
		fscanf(dados, "%[^@]@", p[i].nomeVencedora );
		fscanf(dados, "%[^@]@", p[i].placarAzul );
		fscanf(dados, "%[^@]@", p[i].placarVermelha );
		fscanf(dados, "%[^@]@", p[i].apelidoMVP );

		printf("%s\n", p[i].codigo);
		printf("%s\n", p[i].nomeAzul);
		printf("%s\n", p[i].nomeVermelha);
		printf("%s\n", p[i].data);
		printf("%s\n", p[i].duracao);
		printf("%s\n", p[i].nomeVencedora);
		printf("%s\n", p[i].placarAzul);
		printf("%s\n", p[i].placarVermelha);
		printf("%s\n\n", p[i].apelidoMVP);	}
}
int buscaCodigo(char pk[], indicePrimario p_l[], int tam_p, Partida p[]) {
	int i;

	for (i = 0; i < tam_p; i++) {
		if (strcmp(p_l[i].pk, pk) == 0 && p_l[i].rrn != -1) {
			imprimeRegistro(p_l[i].rrn, p);
			return 1;
		}
	}

	return 0;
}

int buscaVencedora(char vencedora[], indiceWinner w_l[], int tam_w, indicePrimario p_l[], int tam_p, Partida p[]) {
	int i;
	int encontrou = 0;

	for (i = 0; i < tam_w; i++) {
		if (strcmp(w_l[i].vencedora, vencedora) == 0) {
			if (buscaCodigo(w_l[i].pk_registro, p_l, tam_p, p))
				encontrou = 1;
		}
	}

	return encontrou;
}

int buscaMVP(char apelidoMVP[], indiceMVP mvp[], int tam_m, indicePrimario p_l[], int tam_p, Partida p[]) {
	int i;
	int encontrou = 0;

	for (i = 0; i < tam_m; i++) {
		if (strcmp(mvp[i].apelido, apelidoMVP) == 0) {
			if (buscaCodigo(mvp[i].pkRegistro, p_l, tam_p, p))
				encontrou = 1;
		}
	}

	return encontrou;
}

int buscaBinaria(indicePrimario Lista[], char valor[], int inicio, int fim)
{
	int meio;

	meio = (fim-inicio)/2 + inicio;//pega o elemento central

	if(inicio>fim)//não encontrou o elemento
		return -1;
	else
	{
		if (strcmp(Lista[meio].pk, valor) == 0)
			 return(meio);
		else
		{
				if (strcmp(Lista[meio].pk, valor) > 0)
				{
					fim = meio-1;
					return buscaBinaria(Lista,valor,inicio,fim);
				}
				else
				{
					inicio = meio+1;
					return buscaBinaria(Lista,valor,inicio,fim);
				}
		}
	}
}

int listarCodigo(indicePrimario p_l[], int tam_p, Partida p[]) {
	int i, encontrou = 0;

	for (i = 0; i < tam_p; i++) {
		if (p_l[i].rrn != -1){
			imprimeRegistro(p_l[i].rrn, p);
			encontrou++;
		}
	}

	return encontrou;
}

int listarNomeVenc(indiceWinner p_w[], indicePrimario p_l[], int tam_p, Partida p[], int tam_w) {
	int i;
	int encontrou = 0;

	for (i = 0; i < tam_w; i++)
		if (buscaCodigo(p_w[i].pk_registro, p_l, tam_p, p))
				encontrou++;
	return encontrou;
}

int listarNomeMVP(indiceMVP p_m[], indicePrimario p_l[], int tam_p, Partida p[], int tam_m) {
	int i;
	int encontrou = 0;

	for (i = 0; i < tam_m; i++)
		if (buscaCodigo(p_m[i].pkRegistro, p_l, tam_p, p))
				encontrou++;
	return encontrou;
}

void finalizar(indicePrimario p_l[], indiceMVP p_m[], indiceWinner p_w[], int tam_p, int tam_w, int tam_m) {

	salvarIndicePrimario(p_l, &tam_p);
	salvarIndiceWinner(p_w, &tam_w);
	salvarIndiceMVP(p_m, &tam_m);

}
