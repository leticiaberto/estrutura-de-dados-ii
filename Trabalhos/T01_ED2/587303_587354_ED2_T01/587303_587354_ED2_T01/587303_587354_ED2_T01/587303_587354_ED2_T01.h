#ifndef ED2T01_H_INCLUDED
#define ED2_T01_H_INCLUDED


typedef struct partida {
	char nomeAzul[40];
	char nomeVermelha[40];
	char nomeVencedora[40];
	char apelidoMVP[40];
	char placarAzul[3];
	char placarVermelha[3];
	char codigo[9];
	char data[11];
	char duracao[6];
} Partida;

/*iprimary.idx -- ındice primario, contendo a chave prim ́aria e o RRN do respectivo registro,
ordenado pela chave primaria.*/
typedef struct _indicePrimario {
	char pk[9];
	int rrn;
} indicePrimario;

/*iwinner.idx -- ındice secundario, contendo o nome da equipe vencedora e a chave primaria do
respectivo registro, ordenado pelo nome da equipe vencedora.*/
typedef struct _indiceWinner {
	char vencedora[39];
	char pk_registro[9];
} indiceWinner;

/*imvp.idx -- ındice secundario, contendo o apelido do MVP e a chave prim ́aria do respectivo
registro, ordenado pelo apelido do MVP.*/
typedef struct _indiceMVP {
	char apelido[39];
	char pkRegistro[9];
} indiceMVP;


void inicializar(indicePrimario p_l[], indiceWinner p_w[], indiceMVP p_m[], int *tam_p, int *tam_w, int *tam_m, int *indice);
void carregarIndicePrimario(FILE *primary, indicePrimario p_l[], int *tam_p);
void carregarIndiceWinner(FILE *winner, indiceWinner p_w[], int *tam_w);
void carregarIndiceMVP(FILE *mvp, indiceMVP p_m[], int *tam_m);
void refazerIndicesPrimarios (indicePrimario pri[], int *tam_p, FILE *data, int *indice);
void refazerIndicesWinner (indiceWinner w[], int *tam_w, FILE *data);
void refazerIndicesMVP (indiceMVP m[], int *tam_m, FILE *data);
void salvarIndicePrimario(indicePrimario pri[], int *tam_p);
void salvarIndiceWinner(indiceWinner wi[], int *tam_w);
void salvarIndiceMVP(indiceMVP mvp[], int *tam_m);
void liberarEspaco(indicePrimario p_l[], indiceMVP p_m[], indiceWinner p_w[], int tam_p, int tam_w, int tam_m, int *indice);
int verificaPKExistente(indicePrimario p_l[], Partida p[], int *tam_p, int indice);
void listaPrimario (indicePrimario p_l[], Partida p[], int *tam_p, int indice);
void listaWinner (indiceWinner p_l[], Partida p[], int *tam_w, int indice);
void listaMVP (indiceMVP p_l[], Partida p[], int *tam_m, int indice);
int buscaBinaria(indicePrimario Lista[], char valor[], int inicio, int fim);
void imprimeRegistro(int rrn, Partida p[]);

void cadastro(Partida p[], int tam, indicePrimario p_l[], indiceWinner p_w[], indiceMVP p_m[], int *tam_p, int indice, int *tam_w, int *tam_m);
void alterar(char pk[], indicePrimario p_l[], int tam_p, Partida p[]);
int remocao(char pk[9], indicePrimario p_l[], int tam_p);
int buscaCodigo(char pk[], indicePrimario p_l[], int tam_p, Partida p[]);
int buscaVencedora(char vencedora[], indiceWinner w_l[], int tam_w, indicePrimario p_l[], int tam_p, Partida p[]);
int buscaMVP(char apelidoMVP[], indiceMVP mvp[], int tam_m, indicePrimario p_l[], int tam_p, Partida p[]);
int listarCodigo(indicePrimario p_l[], int tam_p, Partida p[]);
int listarNomeVenc(indiceWinner p_w[], indicePrimario p_l[], int tam_p, Partida p[], int tam_w);
int listarNomeMVP(indiceMVP p_m[], indicePrimario p_l[], int tam_p, Partida p[], int tam_m);
void finalizar(indicePrimario p_l[], indiceMVP p_m[], indiceWinner p_w[], int tam_p, int tam_w, int tam_m);




#endif
