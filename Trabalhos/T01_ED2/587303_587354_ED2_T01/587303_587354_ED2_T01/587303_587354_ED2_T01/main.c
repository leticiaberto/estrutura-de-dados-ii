#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "587303_587354_ED2_T01.h"

#define INVALIDO "Campo inválido! Informe novamente: "
#define ERRO "ERRO: Já existe um registro com a chave primária %s.\n"
int main () {

	indicePrimario pri[1000];
	indiceWinner win[1000];
	indiceMVP mvp[1000];
	Partida p[1000];
	int fixo = 36; //data+duracao+chave+placares+separadores(9)
	int tam; //para calcular tamanho total dos campos nao fixos
	int opc;
	int flag = 0;
	char data[11], duracao[6];
	int tam_p = 0, tam_w = 0, tam_m = 0;
	int indice = 0;
	char chave[9];
	int b, list;
	char nomeVencedora[40], apelidoMVP[40];

	inicializar(pri, win, mvp, &tam_p, &tam_w, &tam_m, &indice);

	do {

		scanf ("%d%*c", &opc);

		switch (opc) {
			case 1:

				flag = 0;
				tam = 0;
				data[0] = '\0';

				while (flag == 0) {
					scanf("%[^\n]%*c", p[indice].nomeAzul);
					if (strlen(p[indice].nomeAzul) > 39) {
						printf(INVALIDO);
						flag = 0;
					}
					else
						flag = 1;
				}
				tam = tam + strlen(p[indice].nomeAzul);

				flag = 0;
				while (flag == 0) {
					scanf("%[^\n]%*c", p[indice].nomeVermelha);
					if (strlen(p[indice].nomeVermelha) > 39) {
						printf(INVALIDO);
						flag = 0;
					}
					else
						flag = 1;
				}
				tam = tam + strlen(p[indice].nomeVermelha);

				flag = 0;
				while (flag == 0) {
					scanf("%[^\n]%*c", data);

					if (strlen(data) != 10 || data[2] != '/' || data[5] != '/') {
						printf(INVALIDO);
						flag = 0;
					}
					else {
						int dia, mes, ano;
						char data1[11];
						strcpy(data1, data);
						data[2] = '\0';
						data[5] = '\0';
						dia = atoi(data);
						mes = atoi(data + 3);
						ano = atoi(data + 6);

						if (dia > 31 || dia < 1 || mes > 12 || mes < 1 || ano > 2015 || ano < 2011) {
							printf(INVALIDO);
							flag = 0;
						}
						else
							flag = 1;
							data[10] = '\0';
							strcpy(p[indice].data, data1);
					}
				}

				flag = 0;
				while(flag == 0) {
					scanf("%[^\n]%*c", duracao);
					if (strlen(duracao) != 5 || duracao[2] != ':') {
						printf(INVALIDO);
						flag = 0;
					}
					else {
						flag = 1;
						duracao[5] = '\0';
						strcpy(p[indice].duracao, duracao);
					}

				}

				flag = 0;
				while(flag == 0) {

					scanf("%[^\n]%*c", p[indice].nomeVencedora);
					if (strlen(p[indice].nomeVencedora) <= 39 && (strcmp(p[indice].nomeVencedora, p[indice].nomeAzul) == 0 || strcmp(p[indice].nomeVencedora, p[indice].nomeVermelha) == 0))
						flag = 1;
					else {
						printf(INVALIDO);
						flag = 0;
					}
				}
				tam = tam + strlen(p[indice].nomeVencedora);

				flag = 0;
				while(flag == 0) {
					scanf("%[^\n]%*c", p[indice].placarAzul);
					if (strlen(p[indice].placarAzul) != 2) {
						printf(INVALIDO);
						flag = 0;
					}
					else {
						flag = 1;
						p[indice].placarAzul[2] = '\0';
					}
				}


				flag = 0;
				while(flag == 0) {
					scanf("%[^\n]%*c", p[indice].placarVermelha);
					if (strlen(p[indice].placarVermelha) != 2) {
						printf(INVALIDO);
						flag = 0;
					}
					else {
						flag = 1;
						p[indice].placarVermelha[2] = '\0';
					}
				}

				flag = 0;
				while(flag == 0) {
				scanf("%[^\n]%*c", p[indice].apelidoMVP);
					if (strlen(p[indice].apelidoMVP) > 39) {
						printf(INVALIDO);
						flag = 0;
					}
					else
						flag = 1;
				}
				tam = tam + strlen(p[indice].apelidoMVP);


				int i =0;
				char c;
				strcpy(p[indice].codigo, "");
				strncat(p[indice].codigo, p[indice].nomeAzul, 1);
				strncat(p[indice].codigo, p[indice].nomeVermelha, 1);
				strncat(p[indice].codigo, p[indice].apelidoMVP, 2);

				while (i < 4) {
					c = p[indice].codigo[i];
					p[indice].codigo[i] = toupper(c);
					i++;
				}
				strncat(p[indice].codigo, data,2);//dia
				strncat(p[indice].codigo, (data + 3),2);//mes
				p[indice].codigo[8] = '\0';
				if (verificaPKExistente(pri, p, &tam_p, indice)) {
					printf("ERRO: Já existe um registro com a chave primária: %s.\n",p[indice].codigo);
					break;
				} else {
					tam = tam + fixo;

					cadastro(p, tam, pri, win, mvp, &tam_p, indice, &tam_w, &tam_m);
					tam_p++;
					tam_w++;
					tam_m++;
					indice++;
				}
				break;
			case 2:
				scanf("%[^\n]%*c", chave);
				alterar(chave, pri, tam_p, p);
				break;
			case 3:
				scanf("%[^\n]%*c", chave);
				if (remocao(chave, pri, tam_p)) {}
				else
					printf("Registro não encontrado!\n");
				break;
			case 4:
				scanf("%d\n", &b);
				if(b == 1)//código
				{
					scanf("%[^\n]%*c", chave);
					if(buscaCodigo(chave, pri, tam_p, p)){}
					else
						printf("Registro não encontrado!\n");
				}
				else if(b == 2){
					scanf("%[^\n]%*c", nomeVencedora);
					if(buscaVencedora(nomeVencedora, win, tam_w, pri, tam_p, p) != 0){}//encontrou
					else
						printf("Registro não encontrado!\n");
				}//equipe vencedora
				else if (b == 3){
					scanf("%[^\n]%*c", apelidoMVP);
					if(buscaMVP(apelidoMVP, mvp, tam_m, pri, tam_p, p) != 0){}//encontrou
					else
						printf("Registro não encontrado!\n");
				}//apelido MVP

				break;
			case 5:
				scanf("%d", &list);
				switch(list) {
					case 1:	//listagem por ordem lexicografica de codigo (ja inserimos ordenado)
						if ((listarCodigo(pri, tam_p, p)) == 0)
							printf("Arquivo vazio!\n");
						break;
					case 2:
						if ((listarNomeVenc(win, pri, tam_p, p, tam_w)) == 0)
							printf("Arquivo vazio!\n");
						break;
					case 3:
						if ((listarNomeMVP(mvp, pri, tam_p, p, tam_m)) == 0)
							printf("Arquivo vazio!\n");
						break;
				}
				break;
			case 6:
				liberarEspaco(pri, mvp, win, tam_p, tam_w, tam_m, &indice);
				break;
			case 7:
				finalizar(pri, mvp, win, tam_p, tam_w, tam_m);
				break;
			default:
				break;
		}

	} while (opc != 7);

	return 0;
}
