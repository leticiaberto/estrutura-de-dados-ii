/* ==========================================================================
 * Universidade Federal de São Carlos - Campus Sorocaba
 * Disciplina: Estruturas de Dados 2
 * Prof. Tiago A. de Almeida
 *
 * Trabalho 02 - Árvore B
 *
 * RA: 587354
 * Aluno: Letícia Mara Berto
 * ========================================================================== */

/* Bibliotecas */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

/* Tamanho dos campos dos registros */
#define TAM_PRIMARY_KEY 9
#define TAM_EQUIPE 40
#define TAM_DATA 11
#define TAM_DURACAO 6
#define TAM_PLACAR 3
#define TAM_MVP 40

#define TAM_REGISTRO 192
#define MAX_REGISTROS 1000
#define TAM_ARQUIVO (MAX_REGISTROS * TAM_REGISTRO + 1)

/* Saídas do usuário */
#define OPCAO_INVALIDA "Opcao invalida!\n"
#define MEMORIA_INSUFICIENTE "Memoria insuficiente!"
#define REGISTRO_N_ENCONTRADO "Registro nao encontrado!\n\n"
#define CAMPO_INVALIDO "Campo invalido! Informe novamente.\n"
#define ERRO_PK_REPETIDA "ERRO: Ja existe um registro com a chave primaria: %s.\n"
#define ARQUIVO_VAZIO "Arquivo vazio!"
#define NOS_PERCORRIDOS "Busca por %s. Nos percorridos:\n"

/* Registro da partida */
typedef struct {
	char pk[TAM_PRIMARY_KEY];
	char equipe_azul[TAM_EQUIPE];
	char equipe_vermelha[TAM_EQUIPE];
	char data_partida[TAM_DATA];	// DD/MM/AAAA
	char duracao[TAM_DURACAO];			// MM:SS
	char vencedor[TAM_EQUIPE];
	char placar1[TAM_PLACAR];
	char placar2[TAM_PLACAR];
	char mvp[TAM_MVP];
} Partida;

/* Registro da Árvore-B
 * Contém a chave primária e o RRN do respectivo registro */
typedef struct {
	char pk[TAM_PRIMARY_KEY];	// chave primária
	int rrn;					// rrn do registro
} Chave;

/* Estrutura da Árvore-B */
typedef struct node node_Btree;
struct node {
	int num_chaves;		// numero de chaves armazenadas
	Chave *chave;		// vetor das chaves e rrns [m-1]
	node_Btree **desc;	// ponteiros para os descendentes, *desc[m]
	int folha;			// flag folha da arvore
};
typedef struct {
	node_Btree *raiz;
} Iprimary;

/* Registro do índice secundário
 * Contém o nome da equipe vencedora e a chave primária do registro */
typedef struct {
	char vencedor[TAM_EQUIPE];
	char pk[TAM_PRIMARY_KEY];
} Iwinner;

/* Registro do índice secundário
 * Contém o apelido do MVP e a chave primária do registro */
typedef struct {
	char mvp[TAM_MVP];
	char pk[TAM_PRIMARY_KEY];
} Imvp;

typedef struct {
	Chave *chave;		// chave promovida
	node_Btree *no_filho;		// nó filho da direita
} Retorno;

/* Variáveis globais */
char ARQUIVO[TAM_ARQUIVO];
int M;

/* ==========================================================================
 * ========================= PROTÓTIPOS DAS FUNÇÕES =========================
 * ========================================================================== */

/* Recebe do usuário uma string simulando o arquivo completo e retorna o número
 * de registros. */
int carregar_arquivo();

/* Exibe o jogador */
void exibir_registro(int rrn);

/* <<< DECLARE AQUI OS PROTOTIPOS >>> */
void criar_iprimary(Iprimary *iprimary, int nregistros, int ordem);
void criar_iwinner(Iwinner *iwinner, int nregistros);
void criar_imvp(Imvp *imvp, int nregistros);
void cadastrar(Iprimary *iprimary, Iwinner *iwinner, Imvp *imvp, int *nregistros);
void alterar(Iprimary iprimary);
void buscar(Iprimary iprimary, Iwinner *iwinner, Imvp *imvp, int nregistros);
void listar(Iprimary iprimary, Iwinner *iwinner, Imvp *imvp, int nregistros);
void lista_iprimary(node_Btree *node, int nivel);
void lista_iwinner(Iprimary iprimary, Iwinner *iwinner, int nregistros);
void lista_imvp(Iprimary iprimary, Imvp *imvp, int nregistros);
void apagar_no(node_Btree **iprimary);
Partida recuperar_registro(int rrn);

node_Btree *insere_arvore(node_Btree **no, Chave *chave);
Retorno *insere_arvore_aux(node_Btree **no, Chave *chave);
Retorno *divide_no(node_Btree **no, Chave *chave, node_Btree *no_filho);
Chave *busca_arvore(node_Btree *raiz, char *pk, int imprime);

void insere_listaWinner(Iwinner *lista, Partida *part, int *numRegistros);
void insere_listaMVP(Imvp *lista, Partida *part, int *numRegistros);

Iwinner *busca_listaWinner(Iprimary iprimary, Iwinner *lista, char *winner, int nregistros);
Imvp *busca_listaMVP(Iprimary iprimary, Imvp *lista, char *mvp, int nregistros);

void ignore();

/* ==========================================================================
 * ============================ FUNÇÃO PRINCIPAL ============================
 * =============================== NÃO ALTERAR ============================== */
int main() {

	/* Arquivo */
	int carregarArquivo = 0, nregistros = 0;
	scanf("%d\n", &carregarArquivo); // 1 (sim) | 0 (nao)
	if (carregarArquivo) {
		nregistros = carregar_arquivo();
	}


	/* Índice primário */
	int M;
	scanf("%d", &M);
	Iprimary iprimary;
	criar_iprimary(&iprimary, nregistros, M);


	/* Índice secundário de vencedores */
	Iwinner *iwinner = (Iwinner *) malloc (MAX_REGISTROS * sizeof(Iwinner));
	if (!iwinner) {
		perror(MEMORIA_INSUFICIENTE);
		exit(1);
	}
	criar_iwinner(iwinner, nregistros);


	/* Índice secundário de MVPs */
	Imvp *imvp = (Imvp *) malloc (MAX_REGISTROS * sizeof(Imvp));
	if (!imvp) {
		perror(MEMORIA_INSUFICIENTE);
		exit(1);
	}
	criar_imvp(imvp, nregistros);


	/* Execução do programa */
	int opcao = 0;
	while(opcao != 5) {
		scanf("%d", &opcao);
		switch(opcao) {

		case 1:
			getchar();
			cadastrar(&iprimary, iwinner, imvp, &nregistros);
			break;
		case 2:
			getchar();
			alterar(iprimary);
			break;
		case 3:
			buscar(iprimary, iwinner, imvp, nregistros);
			break;
		case 4:
			listar(iprimary, iwinner, imvp, nregistros);
			break;

		case 5: /* Libera memoria alocada */
			apagar_no(&iprimary.raiz);
			free(iwinner);
			free(imvp);
			break;

		case 10:
			printf("%s\n", ARQUIVO);
			break;

		default:
			ignore();
			printf(OPCAO_INVALIDA);
			break;
		}
	}
	return 0;
}


/* ==========================================================================
 * ================================= FUNÇÕES ================================
 * ========================================================================== */

/* Recebe do usuário uma string simulando o arquivo completo e retorna o número
 * de registros. */
int carregar_arquivo() {
	scanf("%[^\n]\n", ARQUIVO);
	return strlen(ARQUIVO) / TAM_REGISTRO;
}

/* Exibe a partida */
void exibir_registro(int rrn) {

	Partida j = recuperar_registro(rrn);

	printf("%s\n", j.pk);
	printf("%s\n", j.equipe_azul);
	printf("%s\n", j.equipe_vermelha);
	printf("%s\n", j.data_partida);
	printf("%s\n", j.duracao);
	printf("%s\n", j.vencedor);
	printf("%s\n", j.placar1);
	printf("%s\n", j.placar2);
	printf("%s\n", j.mvp);
	printf("\n");
}

/* <<< IMPLEMENTE AQUI AS FUNCOES >>> */

void criar_iprimary(Iprimary *iprimary, int nregistros, int ordem) {

	M = ordem;
	int rrn = 0;

	iprimary->raiz = NULL;

	Partida *part = (Partida *) malloc (sizeof(Partida));
	char *p = ARQUIVO;

	while (sscanf(p, "%[^@]", part->pk) != EOF) {
		sscanf(p, "%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@", part->pk, part->equipe_azul, part->equipe_vermelha, part->data_partida, part->duracao, part->vencedor, part->placar1, part->placar2, part->mvp);
		Chave *chave = (Chave *) malloc (sizeof (Chave));

		strcpy(chave->pk, part->pk);
		chave->rrn = rrn;
		insere_arvore(&iprimary->raiz, chave);
		rrn = rrn + 192;
		p = p + 192;
	}
}

void criar_iwinner(Iwinner *iwinner, int nregistros) {
	char *p = ARQUIVO; //Equivalente ao seek

	Partida *part;
	int tamRegistro = 36;	//comeca com 36 porque eh o tamanho dos campos fixos + @
	int aux;
	int registrosInseridos = 0;

	part = (Partida *)malloc(sizeof(Partida));

	while (sscanf(p, "%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@", part->pk, part->equipe_azul, part->equipe_vermelha, part->data_partida, part->duracao, part->vencedor, part->placar1, part->placar2, part->mvp) != EOF) {
		tamRegistro = 36;

		/*Inserir Ordenado*/
		insere_listaWinner(iwinner, part, &registrosInseridos);
		p = p + 192;
	}
}

void criar_imvp(Imvp *imvp, int nregistros) {
	char *p = ARQUIVO;//Equivalente ao seek

	Partida *part;
	int tamRegistro = 36;	//comeca com 36 porque eh o tamanho dos campos fixos + @
	int aux;
	int registrosInseridos = 0;

	part = (Partida *)malloc(sizeof(Partida));

	while (sscanf(p, "%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@", part->pk, part->equipe_azul, part->equipe_vermelha, part->data_partida, part->duracao, part->vencedor, part->placar1, part->placar2, part->mvp) != EOF) {
		tamRegistro = 36;

		/*Inserir Ordenado*/
		insere_listaMVP(imvp, part, &registrosInseridos);
		p = p + 192;
	}

}

void cadastrar(Iprimary *iprimary, Iwinner *iwinner, Imvp *imvp, int *nregistros) {

	if (*nregistros < MAX_REGISTROS){
		char novo[TAM_REGISTRO];
		novo[0] = '\0';
		int fixo = 36; //data+duracao+chave+placares+separadores(9)
		int tam; //para calcular tamanho total dos campos nao fixos
		char data[11], duracao[6];
		char nomeVencedora[40], apelidoMVP[40];
		char chave[9];
		int flag = 0;
		tam = 0;
		data[0] = '\0';


		Partida *p = (Partida *) malloc (sizeof(Partida));

		while (flag == 0) {
			scanf("%[^\n]%*c", p->equipe_azul);
			if (strlen(p->equipe_azul) > 39) {
				printf(CAMPO_INVALIDO);
				flag = 0;
			}
			else
				flag = 1;
		}
		tam = tam + strlen(p->equipe_azul);

		flag = 0;
		while (flag == 0) {
			scanf("%[^\n]%*c", p->equipe_vermelha);
			if (strlen(p->equipe_vermelha) > 39) {
				printf(CAMPO_INVALIDO);
				flag = 0;
			}
			else
				flag = 1;
		}
		tam = tam + strlen(p->equipe_vermelha);

		flag = 0;
		while (flag == 0) {
			scanf("%[^\n]%*c", data);

			if (strlen(data) != 10 || data[2] != '/' || data[5] != '/' || isalpha(data[0]) || isalpha(data[1]) || isalpha(data[3]) || isalpha(data[4]) || isalpha(data[6]) || isalpha(data[7]) || isalpha(data[8]) || isalpha(data[9])) {
				printf(CAMPO_INVALIDO);
				flag = 0;
			}
			else {
				int dia, mes, ano;
				char data1[11];
				strcpy(data1, data);
				data[2] = '\0';
				data[5] = '\0';
				dia = atoi(data);
				mes = atoi(data + 3);
				ano = atoi(data + 6);

				if (dia > 31 || dia < 1 || mes > 12 || mes < 1 || ano > 2015 || ano < 2011) {
					printf(CAMPO_INVALIDO);
					flag = 0;
				}
				else
					flag = 1;
					data[10] = '\0';
					strcpy(p->data_partida, data1);
			}
		}

		flag = 0;
		while(flag == 0) {
			scanf("%[^\n]%*c", duracao);
			if (strlen(duracao) != 5 || duracao[2] != ':' || isalpha(duracao[0]) || isalpha(duracao[1]) || isalpha(duracao[3]) || isalpha(duracao[4])) {
				printf(CAMPO_INVALIDO);
				flag = 0;
			}
			else {
				flag = 1;
				duracao[5] = '\0';
				strcpy(p->duracao, duracao);
			}

		}

		flag = 0;
		while(flag == 0) {

			scanf("%[^\n]%*c", p->vencedor);
			if (strlen(p->vencedor) <= 39 && (strcmp(p->vencedor, p->equipe_azul) == 0 || strcmp(p->vencedor, p->equipe_vermelha) == 0))
				flag = 1;
			else {
				printf(CAMPO_INVALIDO);
				flag = 0;
			}
		}
		tam = tam + strlen(p->vencedor);

		flag = 0;
		while(flag == 0) {
			scanf("%[^\n]%*c", p->placar1);
			if (strlen(p->placar1) != 2 || isalpha(p->placar1[0]) || isalpha(p->placar1[1])) {
				printf(CAMPO_INVALIDO);
				flag = 0;
			}
			else {
				flag = 1;
				p->placar1[2] = '\0';
			}
		}


		flag = 0;
		while(flag == 0) {
			scanf("%[^\n]%*c", p->placar2);
			if (strlen(p->placar2) != 2 || isalpha(p->placar1[0]) || isalpha(p->placar1[1])) {
				printf(CAMPO_INVALIDO);
				flag = 0;
			}
			else {
				flag = 1;
				p->placar2[2] = '\0';
			}
		}

		flag = 0;
		while(flag == 0) {
		scanf("%[^\n]%*c", p->mvp);
			if (strlen(p->mvp) > 39) {
				printf(CAMPO_INVALIDO);
				flag = 0;
			}
			else
				flag = 1;
		}
		tam = tam + strlen(p->mvp);

		int i =0;
		char c;
		strcpy(p->pk, "");
		strncat(p->pk, p->equipe_azul, 1);
		strncat(p->pk, p->equipe_vermelha, 1);
		strncat(p->pk, p->mvp, 2);

		while (i < 4) {
			c = p->pk[i];
			p->pk[i] = toupper(c);
			i++;
		}
		strncat(p->pk, data,2);//dia
		strncat(p->pk, (data + 3),2);//mes
		p->pk[8] = '\0';

		Chave *res;
		res = busca_arvore(iprimary->raiz, p->pk, 0);
		if (res != NULL) {
			printf(ERRO_PK_REPETIDA, p->pk);
			return;
		}


			tam = tam + fixo;


			strcat(novo,p->pk);
			strcat(novo,"@");
			strcat(novo,p->equipe_azul);
			strcat(novo,"@");
			strcat(novo,p->equipe_vermelha);
			strcat(novo,"@");
			strcat(novo,p->data_partida);
			strcat(novo,"@");
			strcat(novo,p->duracao);
			strcat(novo,"@");
			strcat(novo,p->vencedor);
			strcat(novo,"@");
			strcat(novo,p->placar1);
			strcat(novo,"@");
			strcat(novo,p->placar2);
			strcat(novo,"@");
			strcat(novo,p->mvp);
			strcat(novo,"@");

			for (i = tam; i < TAM_REGISTRO; i++)// < 192
				strcat(novo,"#");
			sprintf(ARQUIVO, "%s%s", ARQUIVO,novo);//insere no arquivo de dados


			Chave *chaave = (Chave *) malloc (sizeof (Chave));
			chaave->rrn = (strlen(ARQUIVO)-192);//calcula o RN com base na quantidade de registros no arquivo de dados ( o -1 é p1 rrn começa do zero)
			strcpy(chaave->pk, p->pk);

			//INSERIR NO ARQUIVO DE DADOS, ARVORE E ATUALIZAR INDICES
			insere_arvore(&iprimary->raiz, chaave);
			int nr = *nregistros;
			insere_listaWinner(iwinner, p, &nr);

			nr = *nregistros;
			insere_listaMVP(imvp, p, &nr);

			(*nregistros)++;

	}
	else
	{
		printf(MEMORIA_INSUFICIENTE);
	}
}

void alterar(Iprimary iprimary) {
	Chave *res;
	Partida part;
	char busca[40], duracao[40];

	scanf("%[^\n]%*c", busca);

	// Busca pelo elemento na arvore
	res = busca_arvore(iprimary.raiz, busca, 0);
	int flag;
	if (res == NULL) {
		printf(REGISTRO_N_ENCONTRADO);
	} else {

		// Recebe um tempo valido
		flag = 0;
		while(flag == 0) {
			scanf("%[^\n]%*c", duracao);
			if (strlen(duracao) != 5 || duracao[2] != ':') {
				printf(CAMPO_INVALIDO);
				flag = 0;
			}
			else {
				flag = 1;
				duracao[5] = '\0';
			}
		}

		// Insere o novo tempo em ARQUIVO
		char *p = ARQUIVO;
		p = p + res->rrn;
		sscanf(p, "%[^@]@%[^@]@%[^@]@%[^@]@", part.pk, part.equipe_azul, part.equipe_vermelha, part.data_partida);

		// Encontra o inicio da escrita do tempo (4 = numero de `@`)
		p = p + 4 + strlen(part.pk) + strlen(part.equipe_azul) + strlen(part.equipe_vermelha) + strlen(part.data_partida);

		sprintf(p, "%s", duracao);

		// Sobrescreve o '\0' escrito pela funcao sprintf
		p = p + 5;
		p[0] = '@';
	}
}

void buscar(Iprimary iprimary, Iwinner *iwinner, Imvp *imvp, int nregistros) {

	char busca[40];
	Chave *res;
	Iwinner *resIwinner;
	Imvp *resImvp;
	int opcao;
	int i;

	scanf("%d", &opcao);
	getchar();
	switch (opcao) {
		case 1: // Busca por chave primaria

			scanf("%[^\n]%*c", busca);
			printf(NOS_PERCORRIDOS, busca);
			res = busca_arvore(iprimary.raiz, busca, 1);
			printf("\n");
			if (res == NULL) {
				printf(REGISTRO_N_ENCONTRADO);
			} else {
				exibir_registro(res->rrn);
			}
			break;

		case 2: // Busca por equipe vencedora
			scanf("%[^\n]%*c", busca);
			busca_listaWinner(iprimary, iwinner, busca, nregistros);
			break;

		case 3: // Busca por MVP
			scanf("%[^\n]%*c", busca);
			busca_listaMVP(iprimary, imvp, busca, nregistros);
			break;

		default:
			printf(OPCAO_INVALIDA);
			break;
	}
}

void listar(Iprimary iprimary, Iwinner *iwinner, Imvp *imvp, int nregistros) {
	int opcao;
	int i;

	scanf("%d", &opcao);
	getchar();
	switch (opcao) {
		case 1: // Listar por chave primaria
			lista_iprimary(iprimary.raiz, 1);
			printf("\n");
			break;

		case 2: // Listar por equipe vencedora
			lista_iwinner(iprimary, iwinner, nregistros);
			break;

		case 3: // Listar por MVP
			lista_imvp(iprimary, imvp, nregistros);
			break;

		default:
			printf(OPCAO_INVALIDA);
			break;
	}
}

void lista_iprimary(node_Btree *node, int nivel) {
	int i;
	for (i = 0; i < M-1; i++) {
		if (node->chave[i].rrn == -1) {
			break;
		}
		if (i == 0) {
			printf("%d - %s", nivel, node->chave[i].pk);
		} else {
			printf(", %s", node->chave[i].pk);
		}
	}
	printf("\n");
	if (!node->folha) {
		for (i = 0; i < M; i++) {
			if (node->desc[i] == NULL) {
				break;
			} else {
				lista_iprimary(node->desc[i], nivel + 1);
			}
		}
	}
}

void lista_iwinner(Iprimary iprimary, Iwinner *iwinner, int nregistros) {
	int i;
	Chave *res;
	for (i = 0; i < nregistros; i++) {
		// Busca pelo rrn na arvore
		res = busca_arvore(iprimary.raiz, iwinner[i].pk, 0);
		//printf("%d\n", res->rrn);
		exibir_registro(res->rrn);
	}
}

void lista_imvp(Iprimary iprimary, Imvp *imvp, int nregistros) {
	int i;
	Chave *res;
	for (i = 0; i < nregistros; i++) {
		// Busca pelo rrn na arvore
		res = busca_arvore(iprimary.raiz, imvp[i].pk, 0);
		exibir_registro(res->rrn);
	}
}

void apagar_no(node_Btree **iprimary) {
	int i;
	if (!(*iprimary)->folha) {
		for (i = 0; i < M-1; i++) {
			if ((*iprimary)->chave[i].rrn == -1) {
				break;
			}
			apagar_no(&(*iprimary)->desc[i]);
		}
		apagar_no(&(*iprimary)->desc[i]);
	}

	free((*iprimary)->chave);
	free(*iprimary);
}

Partida recuperar_registro(int rrn) {
	Partida part;
	char *p = ARQUIVO;
	p = p + rrn;
	sscanf(p, "%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@", part.pk, part.equipe_azul, part.equipe_vermelha, part.data_partida, part.duracao, part.vencedor, part.placar1, part.placar2, part.mvp);
	return part;
}


node_Btree *insere_arvore(node_Btree **no, Chave *chave) {

	int i;
	int j;

	Retorno *retornado; // Struct que foi retornada
	Retorno *retorno = (Retorno *) malloc (sizeof(Retorno)); // Struct que será retornada
	retorno->chave = NULL;
	retorno->no_filho = NULL;


	// Cria nó e insere elemento no novo nó
	if((*no) == NULL) {
		(*no) = (node_Btree *) malloc (sizeof(node_Btree));
		(*no)->desc = (node_Btree **) malloc (sizeof(node_Btree*)*M);
		(*no)->chave = (Chave *) malloc (sizeof(Chave) * (M -1));
		(*no)->folha = 1;

		for (i = 0; i < M-1; i++) {
			(*no)->chave[i].rrn = -1;
			(*no)->desc[i] = NULL;
		}
		(*no)->desc[i] = NULL;

		(*no)->chave[0].rrn = chave->rrn;
		strcpy((*no)->chave[0].pk, chave->pk);

	} else {
		// Raiz já existente
		retornado = insere_arvore_aux(no, chave);

		if (retornado->chave != NULL) {
			node_Btree *aux = (node_Btree *) malloc (sizeof(node_Btree));
			aux = (node_Btree *) malloc (sizeof(node_Btree));
			aux->desc = (node_Btree **) malloc (sizeof(node_Btree*)*M);
			aux->chave = (Chave *) malloc (sizeof(Chave) * (M -1));
			aux->folha = 0;

			for (i = 0; i < M-1; i++) {
				aux->chave[i].rrn = -1;
				aux->desc[i] = NULL;
			}
			aux->desc[i] = NULL;

			aux->chave[0].rrn = retornado->chave->rrn;
			strcpy(aux->chave[0].pk, retornado->chave->pk);

			aux->desc[0] = (*no);
			aux->desc[1] = retornado->no_filho;

			(*no) = aux;
		}
	}
	return *no;
}

Retorno *insere_arvore_aux(node_Btree **no, Chave *chave) {
	int i, j;
	Retorno *retornado; // Struct que foi retornada
	Retorno *retorno = (Retorno *) malloc (sizeof(Retorno)); // Struct que será retornada
	retorno->chave = NULL;
	retorno->no_filho = NULL;

	if ((*no)->folha) { // Nó já existe e é folha
		if ((*no)->chave[M-2].rrn != -1) {
			// Folha cheia
			return divide_no(no, chave, NULL);

		} else {
			// Insere ordenado nesta folha
			for (i = 0; i < M-1; i++) {
				if ((*no)->chave[i].rrn == -1) {
					strcpy((*no)->chave[i].pk, chave->pk);
					(*no)->chave[i].rrn = chave->rrn;

					return retorno;

				} else if (strcmp(chave->pk, (*no)->chave[i].pk) < 0) {
					for (j = i+1; j < M-1; j++) {
						if ((*no)->chave[j].rrn == -1) {
							for (; j > i; j--) {
								(*no)->chave[j] = (*no)->chave[j-1];
							}
							strcpy((*no)->chave[i].pk, chave->pk);
							(*no)->chave[i].rrn = chave->rrn;

							return retorno;
						}
					}
				}
			}
		}

	} else { // Nó já existe e não é folha
		for (i = 0; i < M-1; i++) {
			if ((*no)->chave[i].rrn == -1) {
				break;
			}
		}
		i--;
		while (i >= 0 && strcmp(chave->pk, (*no)->chave[i].pk) < 0) {
			i--;
		}
		i++;
		retornado = insere_arvore_aux(&((*no)->desc[i]), chave);

		if (retornado->chave != NULL) {
			chave = retornado->chave;

			if ((*no)->chave[M-2].rrn != -1) {
				// No cheio
				retorno = divide_no(no, chave, retornado->no_filho);
				return retorno;

			} else {
				// Insere ordenado neste no
				for (i = 0; i < M-1; i++) {
					if ((*no)->chave[i].rrn == -1) {
						strcpy((*no)->chave[i].pk, chave->pk);
						(*no)->chave[i].rrn = chave->rrn;

						(*no)->desc[i+1] = retornado->no_filho;

						return retorno; // NULL, NULL

					} else if (strcmp(chave->pk, (*no)->chave[i].pk) < 0) {
						for (j = i+1; j < M-1; j++) {
							if ((*no)->chave[j].rrn == -1) {
								for (; j > i; j--) {
									(*no)->chave[j] = (*no)->chave[j-1];
									(*no)->desc[j+1] = (*no)->desc[j];
								}
								strcpy((*no)->chave[i].pk, chave->pk);
								(*no)->chave[i].rrn = chave->rrn;

								(*no)->desc[i+1] = retornado->no_filho;

								return retorno; // NULL, NULL
							}
						}
					}
				}
			}
		} else {
			return retorno; // NULL, NULL
		}
	}
}

Retorno *divide_no(node_Btree **no, Chave *chave, node_Btree *no_filho) {
	int i, j, chave_alocada = 0;
	Retorno *retorno = (Retorno *) malloc (sizeof(Retorno)); // Struct que será retornada
	retorno->chave = (Chave *) malloc (sizeof(Chave));
	retorno->no_filho = NULL;
	Chave *chaves;

	node_Btree *nova = (node_Btree *) malloc (sizeof(node_Btree));
	nova = (node_Btree *) malloc (sizeof(node_Btree));
	nova->desc = (node_Btree **) malloc (sizeof(node_Btree*)*M);
	nova->chave = (Chave *) malloc (sizeof(Chave) * (M - 1));
	nova->folha = (*no)->folha;

	for (i = 0; i < M-1; i++) {
		nova->chave[i].rrn = -1;
		nova->desc[i] = NULL;
	}
	nova->desc[i] = NULL;

	i = M-2;
	for (j = (M-1)/2; j >= 0; j--) {
		if (!chave_alocada && strcmp(chave->pk, (*no)->chave[i].pk) > 0) {
			nova->chave[j] = *chave;
			nova->desc[j+1] = no_filho;
			chave_alocada = 1;
		} else {
			nova->chave[j] = (*no)->chave[i];
			nova->desc[j+1] = (*no)->desc[i+1];
			strcpy((*no)->chave[i].pk, "");
			(*no)->chave[i].rrn = -1;
			(*no)->desc[i+1] = NULL;
			i--;
		}
	}

	if (!chave_alocada) {
		while (i >= 0 && strcmp(chave->pk, (*no)->chave[i].pk) < 0) {
			(*no)->chave[i+1] = (*no)->chave[i];
			(*no)->desc[i+2] = (*no)->desc[i+1];
			i--;
		}
		(*no)->chave[i+1] = *chave;
		(*no)->desc[i+2] = no_filho;
	}

	retorno->chave->rrn = nova->chave[0].rrn;
	strcpy(retorno->chave->pk, nova->chave[0].pk);
	(*no)->desc[(M/2) + 1] = NULL;

	for (i = 0; i < M-1; i++) {
		nova->chave[i] = nova->chave[i+1];
		nova->desc[i] = nova->desc[i+1];
	}
	nova->chave[i-1].rrn = -1;
	nova->desc[i] = NULL;
	nova->chave[(M-1)/2].rrn = -1;

	retorno->no_filho = nova;

	return retorno;
}

Chave *busca_arvore(node_Btree *raiz, char *pk, int imprime) {

	int i;
	if (raiz == NULL)
		return NULL;
	if (imprime) {
		for (i = 0; i < M-1; i++) {
			if (raiz->chave[i].rrn == -1) {
				break;
			}
			if (i == 0)
				printf("%s", raiz->chave[i].pk);
			else
				printf(", %s", raiz->chave[i].pk);
		}
		printf("\n");
	}
	for (i = 0; i < M - 1; i++) {
		if (raiz->chave[i].rrn != -1) {
			if (strcmp(pk, raiz->chave[i].pk) == 0) {
				return (&(*raiz).chave[i]);
			} else if (!raiz->folha && strcmp(pk, raiz->chave[i].pk) < 0) {
				return busca_arvore(raiz->desc[i], pk, imprime);
			} else if (raiz->folha && strcmp(pk, raiz->chave[i].pk) < 0) {
				return NULL;
			}
		} else {
			break;
		}
	}
	if (!raiz->folha) {
		return busca_arvore(raiz->desc[i], pk, imprime);
	} else {
		return NULL;
	}
}

void insere_listaWinner(Iwinner *iwinner, Partida *part, int *numRegistros) {

	int i,j, flag = 1;
	if (*numRegistros == MAX_REGISTROS) {
		printf(MEMORIA_INSUFICIENTE);
		return;
	} else if(*numRegistros != 0){
		for (i = 0; i < *numRegistros; i++)
		{

			if (strcmp (part->vencedor, iwinner[i].vencedor) <= 0) // '<=' para inserir sub-ordenado caso winner seja igual
			{
				if (strcmp (part->vencedor, iwinner[i].vencedor) == 0 && strcmp (part->pk, iwinner[i].pk) > 0) { // Insere sub-ordenado pela pk
					continue;
				}
				for (j = *numRegistros; j >= i; j--)
				{
					strcpy(iwinner[j+1].vencedor, iwinner[j].vencedor);
					strcpy(iwinner[j+1].pk, iwinner[j].pk);
				}
				strcpy(iwinner[i].vencedor, part->vencedor);
				strcpy(iwinner[i].pk, part->pk);
				break;
			}
		}
		if(i == *numRegistros){
			strcpy(iwinner[i].vencedor, part->vencedor);
			strcpy(iwinner[i].pk, part->pk);
		}
	}
	else
	{
		strcpy(iwinner[0].vencedor, part->vencedor);
		strcpy(iwinner[0].pk, part->pk);
	}
	(*numRegistros)++;
}


void insere_listaMVP(Imvp *imvp, Partida *part, int *numRegistros) {

	int i,j, flag = 1;
	if (*numRegistros == MAX_REGISTROS) {
		printf(MEMORIA_INSUFICIENTE);
		return;
	} else if(*numRegistros != 0){
		for (i = 0; i < *numRegistros; i++)
		{

			if (strcmp (part->mvp, imvp[i].mvp) <= 0) // '<=' para inserir sub-ordenado caso mvp seja igual
			{
				if (strcmp (part->mvp, imvp[i].mvp) == 0 && strcmp (part->pk, imvp[i].pk) > 0) { // Insere sub-ordenado pela pk
					continue;
				}
				for (j = *numRegistros; j >= i; j--)
				{
					strcpy(imvp[j+1].mvp, imvp[j].mvp);
					strcpy(imvp[j+1].pk, imvp[j].pk);
				}
				strcpy(imvp[i].mvp, part->mvp);
				strcpy(imvp[i].pk, part->pk);
				break;
			}
		}
		if(i == *numRegistros){
			strcpy(imvp[i].mvp, part->mvp);
			strcpy(imvp[i].pk, part->pk);
		}
	}
	else
	{
		strcpy(imvp[0].mvp, part->mvp);
		strcpy(imvp[0].pk, part->pk);
	}
	(*numRegistros)++;
}

Iwinner *busca_listaWinner(Iprimary iprimary, Iwinner *lista, char *winner, int nregistros) {
	int encontrou = 0, i;
	Chave *res;
	for (i = 0; i < nregistros; i++) {
		if (strcmp(winner, lista[i].vencedor) == 0) {
			res = busca_arvore(iprimary.raiz, lista[i].pk, 0);
			exibir_registro(res->rrn);
			encontrou = 1;
		}
	}
	if (!encontrou) {
		printf(REGISTRO_N_ENCONTRADO);
	}
}

Imvp *busca_listaMVP(Iprimary iprimary, Imvp *lista, char *mvp, int nregistros) {
	int encontrou = 0, i;
	Chave *res;
	for (i = 0; i < nregistros; i++) {
		if (strcmp(mvp, lista[i].mvp) == 0) {
			res = busca_arvore(iprimary.raiz, lista[i].pk, 0);
			exibir_registro(res->rrn);
			encontrou = 1;
		}
	}
	if (!encontrou) {
		printf(REGISTRO_N_ENCONTRADO);
	}
}

/* Descarta o que estiver no buffer de entrada */
void ignore() {
   char c;
   while ((c = getchar()) != '\n' && c != EOF);
}
