/* ==========================================================================
 * Universidade Federal de São Carlos - Campus Sorocaba
 * Disciplina: Estruturas de Dados 2
 * Prof. Tiago A. de Almeida
 *
 * Trabalho 03B - Hashing com encadeamento
 *
 * RA: 587354
 * Aluno: Letícia Mara Berto
 * ========================================================================== */

/* Bibliotecas */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

/* Tamanho dos campos dos registros */
#define TAM_PRIMARY_KEY 9
#define TAM_EQUIPE 40
#define TAM_DATA 11
#define TAM_DURACAO 6
#define TAM_PLACAR 3
#define TAM_MVP 40

#define TAM_REGISTRO 192
#define MAX_REGISTROS 5000
#define TAM_ARQUIVO (MAX_REGISTROS * TAM_REGISTRO + 1)

/* Saídas do usuário */
#define OPCAO_INVALIDA "Opcao invalida!\n\n"
#define REGISTRO_N_ENCONTRADO "Registro nao encontrado!\n\n"
#define CAMPO_INVALIDO "Campo invalido! Informe novamente.\n\n"
#define ERRO_PK_REPETIDA "ERRO: Ja existe um registro com a chave primaria: %s.\n\n"
#define REGISTRO_INSERIDO "Registro %s inserido com sucesso.\n\n"

/* Registro da partida */
typedef struct {
	char pk[TAM_PRIMARY_KEY];
	char equipe_azul[TAM_EQUIPE];
	char equipe_vermelha[TAM_EQUIPE];
	char data_partida[TAM_DATA];	// DD/MM/AAAA
	char duracao[TAM_DURACAO];			// MM:SS
	char vencedor[TAM_EQUIPE];
	char placar1[TAM_PLACAR];
	char placar2[TAM_PLACAR];
	char mvp[TAM_MVP];
} Partida;

/* Registro da Tabela Hash
 * Contém a chave primária, o RRN do registro atual e o ponteiro para o próximo
 * registro. */
typedef struct chave Chave;
struct chave {
	char pk[TAM_PRIMARY_KEY];
	int rrn;
	Chave *prox;
};

/* Estrutura da Tabela Hash */
typedef struct {
  int tam;
  Chave **v;
} Hashtable;

/* Variáveis globais */
char ARQUIVO[TAM_ARQUIVO];


/* ==========================================================================
 * ========================= PROTÓTIPOS DAS FUNÇÕES =========================
 * ========================================================================== */

/* Descarta o que estiver no buffer de entrada */
void ignore();

/* Recebe do usuário uma string simulando o arquivo completo. */
void carregar_arquivo();

/* Exibe o jogador */
void exibir_registro(int rrn);

/* <<< DECLARE AQUI OS PROTOTIPOS >>> */
void criar_tabela(Hashtable *tabela, int tam);
void carregar_tabela(Hashtable *tabela);
int insere_tabela(Hashtable *tabela, Chave *chave);

void cadastrar(Hashtable *tabela);
void alterar(Hashtable tabela);
void buscar(Hashtable tabela);
Chave *buscar_hash(Hashtable *tabela, char *pk);
void remover(Hashtable *tabela);
int remover_hash(Hashtable *tabela, char *pk);

void imprimir_tabela(Hashtable tabela);
void liberar_tabela(Hashtable *tabela);

Partida recuperar_registro(int rrn);

/* ==========================================================================
 * ============================ FUNÇÃO PRINCIPAL ============================
 * =============================== NÃO ALTERAR ============================== */
int main() {

	/* Arquivo */

	int carregarArquivo = 0;
	scanf("%d\n", &carregarArquivo); // 1 (sim) | 0 (nao)
	if (carregarArquivo) carregar_arquivo();



	/* Tabela Hash */
	int tam;
	scanf("%d", &tam);
	tam = prox_primo(tam);

	Hashtable tabela;
	criar_tabela(&tabela, tam);
	if (carregarArquivo) carregar_tabela(&tabela);



	/* Execução do programa */
	int opcao = 0;
	while(opcao != 6) {
		scanf("%d", &opcao);
		switch(opcao) {

		case 1:
			getchar();
			cadastrar(&tabela);
			break;
		case 2:
			getchar();
			alterar(tabela);
			break;
		case 3:
			getchar();
			buscar(tabela);
			break;
		case 4:
			remover(&tabela);
			break;
		case 5:
			imprimir_tabela(tabela);
			break;
		case 6:
			liberar_tabela(&tabela);
			break;

		case 10:
			printf("%s\n", ARQUIVO);
			break;

		default:
			ignore();
			printf(OPCAO_INVALIDA);
			break;
		}
	}
	return 0;
}





/* ==========================================================================
 * ================================= FUNÇÕES ================================
 * ========================================================================== */

/* Descarta o que estiver no buffer de entrada */
void ignore() {
	char c;
	while ((c = getchar()) != '\n' && c != EOF);
}

/* Recebe do usuário uma string simulando o arquivo completo. */
void carregar_arquivo() {
	scanf("%[^\n]\n", ARQUIVO);
}

/* Exibe a partida */
void exibir_registro(int rrn) {

	Partida j = recuperar_registro(rrn);

	printf("%s\n", j.pk);
	printf("%s\n", j.equipe_azul);
	printf("%s\n", j.equipe_vermelha);
	printf("%s\n", j.data_partida);
	printf("%s\n", j.duracao);
	printf("%s\n", j.vencedor);
	printf("%s\n", j.placar1);
	printf("%s\n", j.placar2);
	printf("%s\n", j.mvp);
	printf("\n");
}


/* <<< IMPLEMENTE AQUI AS FUNCOES >>> */
Partida recuperar_registro(int rrn){

	Partida part;
	char *p = ARQUIVO;
	p = p + rrn;
	sscanf(p, "%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@%[^@]@", part.pk, part.equipe_azul, part.equipe_vermelha, part.data_partida, part.duracao, part.vencedor, part.placar1, part.placar2, part.mvp);
	return part;
}

int prox_primo(int tam) {
	int i, flag;

	if (tam == 1) { // Para tam == 1 ou 2
		return 1;
	}

	while (1) {
		flag = 0;
		for (i = 2; i <= tam/2; i++) {
			if (tam % i == 0) {
				flag = 1;
				break;
			}
		}

		if (!flag)
			return tam;
		else
			tam++;
	}
}

void criar_tabela(Hashtable *tabela, int tam) {

	int i;

	tabela->tam = tam;
	tabela->v = (Chave **) malloc (sizeof(Chave*) * tam);
	for (i = 0; i < tam; i++){
		tabela->v[i] = (Chave *) malloc (sizeof(Chave));
		tabela->v[i]->prox = NULL;
		//tabela->v[i] = NULL;
	}
}

void carregar_tabela(Hashtable *tabela) {

	char pk[9];
	char *p = ARQUIVO;
	int rrn = 0;

	while (sscanf(p, "%[^@]", pk) != EOF) {
		sscanf(p, "%[^@]@", pk);
		Chave *chave = (Chave *) malloc (sizeof (Chave));

		strcpy(chave->pk, pk);
		chave->rrn = rrn;
		insere_tabela(tabela, chave);
		rrn = rrn + TAM_REGISTRO;
		p = p + TAM_REGISTRO;
	}
}

//FAZER FUNÇÕES DE INSERIR
int insere_tabela(Hashtable *tabela, Chave *chave){

	int i, pos = 0;

	for (i = 0; i < 8; i++){
		pos = pos + (i+1) * chave->pk[i];
	}

	pos = pos % tabela->tam;

	Chave *aux = tabela->v[pos];
	while((*aux).prox != NULL) {
		if (strcmp(chave->pk, aux->prox->pk) < 0){
			chave->prox = aux->prox;
			aux->prox = chave;
			break;
		}
		else
			aux = aux->prox;
	}
	if((*aux).prox == NULL){
		aux->prox = chave;//maybe colocar chave->prox = null
		chave->prox = NULL;
	}
}

void cadastrar(Hashtable *tabela) { //ARRUMAR CHAMADAS DO INSERIR

	char novo[TAM_REGISTRO];
	novo[0] = '\0';
	int fixo = 36; //data+duracao+chave+placares+separadores(9)
	int tam; //para calcular tamanho total dos campos nao fixos
	char data[11], duracao[6];
	char nomeVencedora[40], apelidoMVP[40];
	char chave[9];
	int flag = 0;
	tam = 0;
	data[0] = '\0';

	Partida *p = (Partida *) malloc (sizeof(Partida));

	while (flag == 0) {
		scanf("%[^\n]%*c", p->equipe_azul);
		if (strlen(p->equipe_azul) > 39) {
			printf(CAMPO_INVALIDO);
			flag = 0;
		}
		else
			flag = 1;
	}
	tam = tam + strlen(p->equipe_azul);

	flag = 0;
	while (flag == 0) {
		scanf("%[^\n]%*c", p->equipe_vermelha);
		if (strlen(p->equipe_vermelha) > 39) {
			printf(CAMPO_INVALIDO);
			flag = 0;
		}
		else
			flag = 1;
	}
	tam = tam + strlen(p->equipe_vermelha);

	flag = 0;
	while (flag == 0) {
		scanf("%[^\n]%*c", data);

		if (strlen(data) != 10 || data[2] != '/' || data[5] != '/' || isalpha(data[0]) || isalpha(data[1]) || isalpha(data[3]) || isalpha(data[4]) || isalpha(data[6]) || isalpha(data[7]) || isalpha(data[8]) || isalpha(data[9])) {
			printf(CAMPO_INVALIDO);
			flag = 0;
		}
		else {
			int dia, mes, ano;
			char data1[11];
			strcpy(data1, data);
			data[2] = '\0';
			data[5] = '\0';
			dia = atoi(data);
			mes = atoi(data + 3);
			ano = atoi(data + 6);

			if (dia > 31 || dia < 1 || mes > 12 || mes < 1 || ano > 2015 || ano < 2011) {
				printf(CAMPO_INVALIDO);
				flag = 0;
			}
			else
				flag = 1;
				data[10] = '\0';
				strcpy(p->data_partida, data1);
		}
	}

	flag = 0;
	while(flag == 0) {
		scanf("%[^\n]%*c", duracao);
		if (strlen(duracao) != 5 || duracao[2] != ':' || isalpha(duracao[0]) || isalpha(duracao[1]) || isalpha(duracao[3]) || isalpha(duracao[4])) {
			printf(CAMPO_INVALIDO);
			flag = 0;
		}
		else {
			flag = 1;
			duracao[5] = '\0';
			strcpy(p->duracao, duracao);
		}

	}

	flag = 0;
	while(flag == 0) {

		scanf("%[^\n]%*c", p->vencedor);
		if (strlen(p->vencedor) <= 39 && (strcmp(p->vencedor, p->equipe_azul) == 0 || strcmp(p->vencedor, p->equipe_vermelha) == 0))
			flag = 1;
		else {
			printf(CAMPO_INVALIDO);
			flag = 0;
		}
	}
	tam = tam + strlen(p->vencedor);

	flag = 0;
	while(flag == 0) {
		scanf("%[^\n]%*c", p->placar1);
		if (strlen(p->placar1) != 2 || isalpha(p->placar1[0]) || isalpha(p->placar1[1])) {
			printf(CAMPO_INVALIDO);
			flag = 0;
		}
		else {
			flag = 1;
			p->placar1[2] = '\0';
		}
	}


	flag = 0;
	while(flag == 0) {
		scanf("%[^\n]%*c", p->placar2);
		if (strlen(p->placar2) != 2 || isalpha(p->placar1[0]) || isalpha(p->placar1[1])) {
			printf(CAMPO_INVALIDO);
			flag = 0;
		}
		else {
			flag = 1;
			p->placar2[2] = '\0';
		}
	}

	flag = 0;
	while(flag == 0) {
	scanf("%[^\n]%*c", p->mvp);
		if (strlen(p->mvp) > 39) {
			printf(CAMPO_INVALIDO);
			flag = 0;
		}
		else
			flag = 1;
	}
	tam = tam + strlen(p->mvp);

	int i =0;
	char c;
	strcpy(p->pk, "");
	strncat(p->pk, p->equipe_azul, 1);
	strncat(p->pk, p->equipe_vermelha, 1);
	strncat(p->pk, p->mvp, 2);

	while (i < 4) {
		c = p->pk[i];
		p->pk[i] = toupper(c);
		i++;
	}
	strncat(p->pk, data,2);//dia
	strncat(p->pk, (data + 3),2);//mes
	p->pk[8] = '\0';

	Chave *res;
	res = buscar_hash(tabela, p->pk);
	if (res != NULL) {
		printf(ERRO_PK_REPETIDA, p->pk);
		return;
	}

		tam = tam + fixo;

		strcat(novo,p->pk);
		strcat(novo,"@");
		strcat(novo,p->equipe_azul);
		strcat(novo,"@");
		strcat(novo,p->equipe_vermelha);
		strcat(novo,"@");
		strcat(novo,p->data_partida);
		strcat(novo,"@");
		strcat(novo,p->duracao);
		strcat(novo,"@");
		strcat(novo,p->vencedor);
		strcat(novo,"@");
		strcat(novo,p->placar1);
		strcat(novo,"@");
		strcat(novo,p->placar2);
		strcat(novo,"@");
		strcat(novo,p->mvp);
		strcat(novo,"@");

		for (i = tam; i < TAM_REGISTRO; i++)// < 192
			strcat(novo,"#");
		sprintf(ARQUIVO, "%s%s", ARQUIVO,novo);//insere no arquivo de dados


		Chave *chaave = (Chave *) malloc (sizeof (Chave));
		chaave->rrn = (strlen(ARQUIVO)-192);//calcula o RN com base na quantidade de registros no arquivo de dados ( o -1 é p1 rrn começa do zero)
		strcpy(chaave->pk, p->pk);

		//INSERIR NO ARQUIVO DE DADOS E ATUALIZAR INDICES
		insere_tabela(tabela, chaave);
		printf(REGISTRO_INSERIDO,chaave->pk);

}

void alterar(Hashtable tabela) {
	Chave *res;
	Partida part;
	char busca[9], duracao[5];

	scanf("%[^\n]%*c", busca);

	// Busca pelo elemento na arvore
	res = buscar_hash(&tabela, busca);
	int flag;
	if (res == NULL) {
		printf(REGISTRO_N_ENCONTRADO);
	} else {

		// Recebe um tempo valido
		flag = 0;
		while(flag == 0) {
			scanf("%[^\n]%*c", duracao);
			if (strlen(duracao) != 5 || duracao[2] != ':' || isalpha(duracao[0]) || isalpha(duracao[1]) || isalpha(duracao[3]) || isalpha(duracao[4])) {
				printf(CAMPO_INVALIDO);
				flag = 0;
			}
			else {
				flag = 1;
				duracao[5] = '\0';
			}
		}

		// Insere o novo tempo em ARQUIVO
		char *p = ARQUIVO;
		p = p + res->rrn;
		sscanf(p, "%[^@]@%[^@]@%[^@]@%[^@]@", part.pk, part.equipe_azul, part.equipe_vermelha, part.data_partida);

		// Encontra o inicio da escrita do tempo (4 = numero de `@`)
		p = p + 4 + strlen(part.pk) + strlen(part.equipe_azul) + strlen(part.equipe_vermelha) + strlen(part.data_partida);

		sprintf(p, "%s", duracao);

		// Sobrescreve o '\0' escrito pela funcao sprintf
		p = p + 5;
		p[0] = '@';
	}
}

void buscar(Hashtable tabela){
	char pk[9];
	scanf("%[^\n]%*c", pk);

	Chave *c;
	c = buscar_hash(&tabela, pk);

	if(c == NULL)
		printf(REGISTRO_N_ENCONTRADO);
	else
		exibir_registro(c->rrn);
}

Chave *buscar_hash(Hashtable *tabela, char *pk){

	int i, pos = 0;
	//Calculo do h(k)
	for (i = 0; i < 8; i++){
		pos = pos + (i+1) * pk[i];
	}
	pos = pos % tabela->tam;

	Chave *aux = tabela->v[pos];

	while((*aux).prox != NULL) {
		if (strcmp(pk, aux->prox->pk) == 0){
			return (aux->prox);
		}
		else
		aux = aux->prox;
	}
	return NULL;
}

void remover(Hashtable *tabela){

	getchar();
	char pk[9];
	scanf("%[^\n]%*c", pk);

	int c;
	c = remover_hash(tabela, pk);

	if(c == -1)
		printf(REGISTRO_N_ENCONTRADO);
	else
	{
		char *p = ARQUIVO;
		p = p + c;//c = rrn
		p[0] = '*';
		p[1] = '|';
	}
}

int remover_hash(Hashtable *tabela, char *pk){

	int i, pos = 0, rrn;
	//Calculo do h(k)
	for (i = 0; i < 8; i++){
		pos = pos + (i+1) * pk[i];
	}
	pos = pos % tabela->tam;

	Chave *aux = tabela->v[pos];

	while((*aux).prox != NULL) {
		if (strcmp(pk, aux->prox->pk) == 0){
			Chave *removido;
			removido = aux->prox;
			rrn = aux->prox->rrn;
			aux->prox = aux->prox->prox;
			free(removido);
			return rrn;
		}
		else
		aux = aux->prox;
	}
	return -1;
}

void imprimir_tabela(Hashtable tabela){

	int i;

	Chave *aux;

	for(i = 0; i < tabela.tam; i++){
		aux = tabela.v[i];
		printf("[%d]", i);
		while((*aux).prox != NULL){
			printf(" %s",aux->prox->pk);
			aux = aux->prox;
		}
		printf("\n");
	}
}

void liberar_tabela(Hashtable *tabela){

	int i;
	Chave *aux, *aux2;

	for(i = 0; i < tabela->tam; i++){
		aux = tabela->v[i];
		while((*aux).prox != NULL){
			aux2 = aux;
			aux = aux->prox;
			free(aux2);
		}
	}
	free(tabela->v);
}
